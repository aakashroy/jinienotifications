"""ActiveMQ helper functions"""

import time
import traceback
import uuid

import stomp

from common import logging


class MyListener(stomp.ConnectionListener):
    """
    Listener Class for ActiveMQ connections
    """

    def __init__(self, activemq_handle, on_message_callback):
        self.__activemq_handle = activemq_handle
        self.__message_id = None
        self.__subscription = None
        self.__on_message_callback = on_message_callback
        super(MyListener, self).__init__()

    def on_before_message(self, headers, body):
        logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "ON-BEFORE-MESSAGE", {"headers": headers})
        self.__message_id = headers['message-id']
        self.__subscription = headers['subscription']

    def on_error(self, headers, body):
        logging.log(logging.SEVERITY_ERROR, "AMQ-LISTENER", "ON-ERROR", {"headers": headers, "message": body})

    def on_message(self, headers, body):
        logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "ON-MESSAGE", {"headers": headers, "message": body})
        try:
            self.__on_message_callback(headers, body, self.__message_id, self.__subscription, self.__activemq_handle)
        except:
            logging.log(logging.SEVERITY_CRITICAL, "AMQ-LISTENER",
                        traceback.format_exc())


def restablish_connection(name, on_message_callback, config, destination_q, old_handle=None, old_subscriber_id=None):
    """
    Restablish ActiveMQ connection
    :param name:
    :param on_message_callback:
    :param config:
    :param destination_q:
    :param old_handle:
    :param old_subscriber_id:
    :return:
    """
    if old_handle is not None:
        try:
            logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "Unsubscribing")
            old_handle.unsubscribe(id=old_subscriber_id)
        except Exception:
            pass
        try:
            logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "Disconnecting")
            old_handle.disconnect()
        except Exception:
            pass

    subscriber_id = uuid.uuid4().hex
    logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "Subscriber ID: " + unicode(subscriber_id))
    activemq_handle = stomp.Connection(config.ACTIVEMQ_CONNECTION)
    activemq_handle.set_listener(name, MyListener(activemq_handle, on_message_callback))
    activemq_handle.start()
    activemq_handle.connect()
    activemq_handle.subscribe(destination=destination_q,
                              id=subscriber_id)
    return activemq_handle, subscriber_id


def run_worker(name, on_message_callback, config, destination_q):
    """
    Function to run worker
    :param name:
    :param on_message_callback:
    :param config:
    :param destination_q:
    :return:
    """
    logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "Running " + name)
    activemq_handle, subscriber_id = restablish_connection(name, on_message_callback, config, destination_q)

    while True:
        time.sleep(0.1)
        reestablish = False
        try:
            if not activemq_handle.is_connected():
                reestablish = True
        except Exception:
            pass
        if reestablish:
            time.sleep(10)
            logging.log(logging.SEVERITY_DEBUG, "AMQ-LISTENER", "Reestablishing connection for " + name)
            activemq_handle, subscriber_id = restablish_connection(name, on_message_callback, config,
                                                                   destination_q, activemq_handle, subscriber_id)
