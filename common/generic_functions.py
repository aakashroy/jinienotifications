"""Generic Functions"""
from datetime import datetime

from dateutil import tz


def convert_datetime_with_zone_to_local(datetimestr, datetimezone):
    """
    Convert datetime with zone to local
    :param datetimestr: Datetime
    :param datetimezone: Datetime zone
    :return:
    """
    try:
        from_zone = tz.gettz(datetimezone)
        to_zone = tz.tzlocal()
        date_obj = datetime.strptime(datetimestr, '%Y-%m-%dT%H:%M:%S.%f0')
        utc = date_obj.replace(tzinfo=from_zone)
        ist = utc.astimezone(to_zone)
        return ist.replace(tzinfo=None).strftime("%-I:%M%p")
    except:
        return None


def get_datetime_from_str(datetimestr, datetimezone):
    """
    Get date time from string
    :param datetimestr: Datetime string
    :param datetimezone: Datetime zone
    :return: Date object
    """
    try:
        from_zone = tz.gettz(datetimezone)
        date_obj = datetime.strptime(datetimestr, '%Y-%m-%dT%H:%M:%S.%f0')
        return date_obj.replace(tzinfo=from_zone).replace(tzinfo=None)
    except:
        return None


def get_datetime_from_str_to_local(datetimestr, datetimezone):
    """
    Get datetime from string to local
    :param datetimestr: Datetime string
    :param datetimezone: Datetime Zone
    :return:
    """

    try:
        from_zone = tz.gettz(datetimezone)
        to_zone = tz.tzlocal()
        date_obj = datetime.strptime(datetimestr, '%Y-%m-%dT%H:%M:%S.%f0')
        utc = date_obj.replace(tzinfo=from_zone)
        ist = utc.astimezone(to_zone)
        return ist.replace(tzinfo=None)
    except:
        return None


def get_greeting(date):
    """
    Get Greeting
    :param date: Date
    :return: Part of the day
    """

    if date.hour < 12:
        return "Morning"
    elif 12 <= date.hour < 16:
        return "Afternoon"

    return "Evening"


def get_first_name(first_name, middle_name, last_name):
    """
    Return the first name basis the first, middle & last names
    :param first_name: First Name
    :param middle_name: Middle Name
    :param last_name: Last Name
    :return:
    """
    first_name = first_name.strip() if first_name else ""
    middle_name = middle_name.strip() if middle_name else ""
    last_name = last_name.strip() if last_name else ""

    if len(first_name) > 2:
        reqd_first_name = first_name
    else:
        if middle_name:
            reqd_first_name = first_name + " " + middle_name + " " + last_name
        else:
            reqd_first_name = first_name + " " + last_name

    return reqd_first_name
