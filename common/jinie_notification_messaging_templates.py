"""Jinie Notification Messagin Templates"""

ONBOARDING = {
    'message_type': -1,
    'destination_alt_user_id': -1,
    'me': '',
    'tokens': ''
}

NOTIFICATION_RESPONSE = {
    'notfn_id': -1,
    'message_type': -1,
    'destination_alt_user_id': -1,
    'message': ''
}

NOTIFICATION_REQUEST = {
    'message_type': -1,
    'destination_alt_user_id': -1,
    'scheduled_time': None
}
