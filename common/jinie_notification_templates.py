"""Jinie Notification Templates"""


def get_jinie_design_label_template(messages):
    """
    Get template for sending HTML formatted text
    :param message:
    :return: The structure recognized by Jinie
    """
    return_dict = {"design": []}
    for message in messages:
        return_dict["design"].append({
            "label": message,
            "type": "TEXT",
            "subType": None,
            "displayDataType": None,
            "selectionType": None,
            "questionID": None,
            "sequence": None,
            "displaySeq": "",
            "cellTrackerID": None,
            "selectionOptions": [

            ],
            "selectedValue": [

            ],
            "flow": None
        })
    return [return_dict]


def get_jinie_design_title_description_template(messages):
    """
    Get template for sending Meeting time start & end
    :param message:
    :return: The structure recognized by Jinie
    """
    return_dict = {"design": []}
    for message in messages:
        return_dict["design"].append({
            "label": None,
            "type": "OUTLOOK",
            "subType": "MEETING_TIME",
            "genericArray": [
                {
                    "metgTitle": message["title"],
                    "metgTime": message["description"]
                }
            ],
            "displayDataType": None,
            "selectionType": None,
            "questionID": None,
            "sequence": None,
            "displaySeq": None,
            "cellTrackerID": None,
            "selectionOptions": [

            ],
            "selectedValue": [

            ],
            "flow": None
        })
    return return_dict


def get_jinie_motd_template(motd_dict):
    """
    Get template for sending MOTD in Jinie
    :param motd_dict: Dictionary of MOTD container values
    :return: The design element (json) recognized by Jinie
    """

    greeting = motd_dict['greeting']
    list_of_meetings = motd_dict['list_of_meetings']
    quote = motd_dict['quote']
    author = motd_dict['author']
    ending = motd_dict['ending']

    return_dict = {"design": [{}]}

    return_dict["design"][0]["type"] = "MOTD"
    return_dict["design"][0]["label"] = greeting
    return_dict["design"][0]["displayDataType"] = None
    return_dict["design"][0]["selectionType"] = None
    return_dict["design"][0]["questionID"] = None
    return_dict["design"][0]["sequence"] = None
    return_dict["design"][0]["displaySeq"] = None
    return_dict["design"][0]["cellTrackerID"] = None
    return_dict["design"][0]["selectionOptions"] = []
    return_dict["design"][0]["selectedValue"] = []
    return_dict["design"][0]["flow"] = None

    generic_array = [{
        "list": [],
        "quote": [
            {
                "title": author,
                "desc": quote
            }
        ],
        "footer": ending
    }]

    for meeting in list_of_meetings:
        generic_array[0]["list"].append({
            "title": meeting["title"],
            "desc": meeting["description"],
            "sep": True
        })

    if generic_array[0]["list"]:
        generic_array[0]["list"][-1]["sep"] = False

    return_dict["design"][0]["genericArray"] = generic_array

    return [return_dict]


MOTD_GREETING = u"<b>Good {0} {1},</b>"
MOTD_UNREAD_MAILS = u"You have <b>{0}</b> unread mail since yesterday."
MOTD_IMP_MAILS = u" <b>{0}</b> {1} marked with high importance."
MOTD_MEETINGS = u"You have <b>{0}</b> meeting{1} today."
MOTD_EACH_MEETING = u" > {0} - {1} <b>{2}</b>"
MOTD_QOTD = u"<b>Food for thought:</b><br/>{0} - <i>{1}</i>"
MOTD_ENDING = u"Have a nice day!"

CALENDAR_GREETING = u"{0}, you have an upcoming meeting!"
CALENDAR_MEETING = u" > {0} - {1} <b>{2}</b>"
CALENDAR_MEETING_TIME = u"{0} - {1}"
CALENDAR_MEETING_ALL_DAY = u"All Day"
CALENDAR_MEETING_LOCATION = u" > <b>Location:</b> {0}"
CALENDAR_REMINDER_MEETING_LOCATION = u"{0}"
CALENDAR_MEETING_PEOPLE = u" > <b>People:</b> {0}"
CALENDAR_LINK = u"Open in Outlook"
CALENDAR_GTM_LINK = u"<a href='{0}'>Join meeting online</a>"
CALENDAR_LATE_LINK = u"I will be late"
CALENDAR_ORG_LATE_LINK = u"Delay meeting by 15 min"
CALENDAR_NOT_GOING_LINK = u"I am not going"
CALENDAR_ENDING = u"<br/>"  # <a href='{0}'>Open in O365</a> TODO: How to do this?

CALENDAR_ACTION_LATE = u"{0} will be <b>late</b> for the <i>{1}</i> meeting."
CALENDAR_ACTION_NOTATTENDING = u"{0} will <b>not attend</b> the <i>{1}</i> meeting."
CALENDAR_ACTION_ORGANIZER_LATE = u"The <i>{0}</i> meeting will start 15 min. late"
