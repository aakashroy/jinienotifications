"""Mongo Helpers"""
import json
import re
import string
import traceback
from datetime import datetime

import pymongo

from common import logging


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def clean_string(inputstr):
    outputstr = inputstr.lower()
    outputstr = translate_punctuation_to_empty(outputstr)
    outputstr = re.sub(r'\s+', ' ', outputstr)
    return outputstr.strip()


def delete_user(conf, alt_id):
    """
    Delete a user from Mongo DB
    :param conf: Configuration details
    :param alt_id: alt id (user id)
    :return:
    """
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.delete_many({"alt_id": alt_id})
        if result:
            return True
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def delete_users(conf, alt_ids):
    """
        Delete a user from Mongo DB
        :param conf: Configuration details
        :param alt_id: alt id (user id)
        :return:
        """
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.delete_many({'alt_id': {'$in': alt_ids}})
        if result:
            return True
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def insert_new_user(conf, org_id, alt_id, emp_id, email, first_name, middle_name, last_name):
    """
    Insert a new user into Mongo DB
    :param conf: Configuration details
    :param alt_id: alt id (user id)
    :param emp_id: employee id
    :param email: email address
    :return:
    """
    user_data = {}
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        first_name = first_name if first_name else ""
        middle_name = middle_name if middle_name else ""
        last_name = last_name if last_name else ""
        user_data = {
            "org_id": org_id,
            "emp_id": emp_id,
            "email": email,
            "alt_id": alt_id,
            "allowed_services": {
                "enabled": True,
                "motd": {
                    "enabled": True,
                    "schedule": "0 8 * * *"
                }
            },
            "first_name": first_name,
            "middle_name": middle_name,
            "last_name": last_name,
            "search_email": email.lower(),
            "search_f_name": clean_string(first_name),
            "search_fm_name": clean_string(first_name + middle_name),
            "search_fl_name": clean_string(first_name + last_name),
            "search_fml_name": clean_string(first_name + middle_name + last_name)
        }
        result = coll.insert_one(user_data)

        if result:
            return True
    except Exception:
        print json.dumps(user_data, indent=4, default=str)
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def fetch_all_user_alt_ids(conf, org_id):
    """
    Fetches all Users (only alt ids) from Mongo
    :param conf: Configuration details
    :return:
    """
    result = None

    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": org_id}, {"alt_id": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def get_user_details_by_alt_id(alt_id, conf):
    """
    Get user details from alt id
    :param alt_id: Altid
    :param conf: Config
    :return: User details
    """
    result = None

    # if alt_id == 2060312:
    #    alt_id = 2205395
    try:
        alt_id = int(alt_id)
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one({"alt_id": alt_id})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def update_user_token_details(alt_id, tokens, conf):
    """
    Update user token details
    :param alt_id: Altid
    :param tokens: Tokens
    :param conf: Config
    :return: Updated Values
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]

        update_values = {}
        update_values["Bearer"] = tokens["token_type"]
        update_values["scopes"] = tokens["scope"]
        update_values["expires_in"] = tokens["expires_in"]
        update_values["ext_expires_in"] = tokens["ext_expires_in"]
        update_values["access_token"] = tokens["access_token"]
        update_values["refresh_token"] = tokens["refresh_token"]
        update_values["id_token"] = tokens["id_token"]
        update_values['updated_on'] = datetime.now()

        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def get_user_details_from_email(email, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one({"email": email})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def get_user_details_from_email_v2(email, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one({"search_email": email})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def update_o365_email_created_webhook_id(alt_id, new_id, creator_id, expdt, conf):
    """
    Update user token details
    :param alt_id: Altid
    :param tokens: Tokens
    :param conf: Config
    :return: Updated Values
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]

        update_values = {}
        update_values["o365_email_created_subscription_id"] = new_id
        update_values["o365_email_created_subscription_creator_id"] = creator_id
        update_values["o365_email_created_subscription_expdt"] = expdt

        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def get_user_details_from_o365_email_created_subscription_creator_id(o365_email_created_subscription_creator_id, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one(
            {"o365_email_created_subscription_creator_id": o365_email_created_subscription_creator_id})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


if __name__ == "__main__":
    # Tests
    from simplex_communications import config

    print get_user_details_by_alt_id("2205395", config)
