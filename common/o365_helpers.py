"""O365 helpers"""
# Note that disable=no-member has been used for pylint because pylint fails to detect
# more nested members
import json
import traceback
import uuid
from datetime import datetime, timedelta

import pytz
import requests
from dateutil import tz

from common import web_helpers, generic_functions, mongo_helpers, logging


def refresh_access_token(refresh_token, config, scopes):
    """
    Refresh Access Token
    :param refresh_token: Refresh Token
    :param config: Config
    :return:
    """
    result = None
    try:
        post_data = {'grant_type': 'refresh_token',
                     'refresh_token': refresh_token,
                     'redirect_uri': config.REDIRECT_URI,
                     # 'scope': ' '.join(unicode(i) for i in config.SCOPES),
                     'scope': scopes,
                     'client_id': config.CLIENT_ID,
                     'client_secret': config.CLIENT_SECRET}

        req = requests.post(config.TOKEN_ENDPOINT, data=post_data)
        result = req.json()
        if 'access_token' not in result:
            logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                        "Received different result from expected: " + unicode(result))
            result = None
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())
    return result


def get_unread_emails(user_details, start_date, end_date, config):
    """
    Get unread emails
    :param user_details: User details
    :param start_date: Start Date
    :param end_date: End Date
    :param config: Config
    :return: Unread Mails
    """
    result = None
    try:
        get_messages_url = config.MS_GRAPH_ENDPOINT.format('/me/mailfolders/inbox/messages')

        query_parameters = {'$filter': 'receivedDateTime ge ' + str(start_date) +
                                       ' and receivedDateTime lt ' + str(end_date) +
                                       ' and isRead eq false ' +
                                       ' and inferenceClassification eq \'focused\'',
                            '$select': 'importance, inferenceClassification'}

        req = web_helpers.make_api_call('GET', get_messages_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
            result['value'], max_calls_reached = get_more_data(result, user_details['access_token'])
            if max_calls_reached:
                result['max_calls_reached'] = max_calls_reached
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())
    return result


def get_datetime_from_date_time_tz(start_date, start_time, timezone=None):
    """
    Get datetime from datetime timezone
    :param start_date: Start Date
    :param start_time: Start Time
    :param timezone: Timezone
    :return: Datetime
    """
    utc = None
    try:
        if timezone is not None:
            from_zone = tz.tzlocal()
            to_zone = tz.gettz(timezone)
            date_obj = datetime.combine(start_date, start_time)
            ist = date_obj.replace(tzinfo=from_zone)
            utc = ist.astimezone(to_zone)
            utc = utc.replace(tzinfo=None)
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    if utc is None:
        utc = datetime.combine(start_date, start_time)

    return utc


def convert_datetime_timezone(dt, source_tz, dest_tz):
    tz_src = pytz.timezone(source_tz)
    tz_dest = pytz.timezone(dest_tz)
    dt = dt.replace(tzinfo=None)
    dt = tz_src.localize(dt)
    dt = dt.astimezone(tz_dest)
    return dt.replace(tzinfo=None)


def pad_datetime_str_for_o365_api_utc(date_part, time_part, timezone="UTC"):
    """
    :param date_part: Only the date object
    :param time_part: Only the time object
    :param timezone: The source Timezone
    :return: The string representation of date/time required by o365 API in UTC
    """
    date_obj = datetime.combine(date_part, time_part)
    utc = convert_datetime_timezone(date_obj, timezone, "UTC")
    start_date_time_str = str(utc).replace(" ", "T")
    start_date_time_str_arr = start_date_time_str.split(".")
    if len(start_date_time_str_arr) > 1:
        start_date_time_str_suffix = start_date_time_str_arr[-1]
    else:
        start_date_time_str_suffix = ""
    if 7 - len(start_date_time_str_suffix) != 0:
        if start_date_time_str_suffix:
            start_date_time_str_suffix = start_date_time_str_suffix[0:7]
        else:
            start_date_time_str_suffix = start_date_time_str_suffix.ljust(7, '0')
    if len(start_date_time_str_arr) > 1:
        start_date_time_str = "".join(start_date_time_str_arr[:-1]) + "." + start_date_time_str_suffix
    else:
        start_date_time_str = start_date_time_str + "." + start_date_time_str_suffix
    return start_date_time_str


def pad_datetime_str_for_o365_api(start_date, start_time, timezone=None):
    """
    Pad datetime string for O365 api
    :param start_date: Start Date
    :param start_time: Start Time
    :param timezone: Timezone
    :return: Datetime
    """
    utc = None
    try:
        if timezone is not None:
            from_zone = tz.tzlocal()
            to_zone = tz.gettz(timezone)
            date_obj = datetime.combine(start_date, start_time)
            ist = date_obj.replace(tzinfo=from_zone)
            utc = ist.astimezone(to_zone)
            utc = utc.replace(tzinfo=None)
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    if utc is None:
        utc = datetime.combine(start_date, start_time)

    start_date_time_str = str(utc).replace(" ", "T")
    start_date_time_str_arr = start_date_time_str.split(".")
    if len(start_date_time_str_arr) > 1:
        start_date_time_str_suffix = start_date_time_str_arr[-1]
    else:
        start_date_time_str_suffix = ""
    if 7 - len(start_date_time_str_suffix) != 0:
        if start_date_time_str_suffix:
            start_date_time_str_suffix = start_date_time_str_suffix[0:7]
        else:
            start_date_time_str_suffix = start_date_time_str_suffix.ljust(7, '0')
    if len(start_date_time_str_arr) > 1:
        start_date_time_str = "".join(start_date_time_str_arr[:-1]) + "." + start_date_time_str_suffix
    else:
        start_date_time_str = start_date_time_str + "." + start_date_time_str_suffix
    return start_date_time_str


def get_event_list(user_details, config):
    """
    Get event list
    :param user_details: User Details
    :param config: Config
    :return: Event List
    """

    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/calendar/events')

        query_parameters = {
            '$orderby': 'start/dateTime DESC'
        }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result


def get_event(event_id, user_details, config,
              select="start,end,subject,location,type,attendees,isCancelled,organizer,reminderMinutesBeforeStart"):
    """
    Get Event
    :param event_id: Event id
    :param user_details: User Details
    :param config: Config
    :param select: parameters to be selected from Dictionary returned by MS_Graph
    :return: Event
    """

    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/events/' + str(event_id))

        if select == "all":
            query_parameters = None
        else:
            query_parameters = {
                '$select': select
            }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result


def get_events(user_details, start_date, end_date, start_time, end_time, config, timezone=None):
    """
    Get Events
    :param user_details: User Details
    :param start_date: Start DateE
    :param end_date: End Date
    :param start_time: Start Time
    :param end_time: End Time
    :param config: Config
    :param timezone: LocalTimezone
    :return: Events
    """
    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/calendar/calendarView')
        if timezone is None:
            timezone = tz.tzlocal()
        query_parameters = {
            'startDateTime': pad_datetime_str_for_o365_api_utc(start_date, start_time, timezone),
            'endDateTime': pad_datetime_str_for_o365_api_utc(end_date, end_time, timezone),
            # Is this end time inclusive?
            '$orderby': 'start/dateTime ASC',
            '$select': 'start,end,subject,location,type,attendees,isCancelled,isOrganizer,webLink,onlineMeetingUrl,isAllDay,reminderMinutesBeforeStart'
        }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)
        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
            result['value'], dummy_max_calls_reached = get_more_data(result, user_details['access_token'])
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result


def get_more_data(results, access_token, max_calls=5):
    """
    Get more data
    :param results: Results
    :param access_token: Access Tokens
    :param max_calls: Max Calls
    :return: Response results, Max calls reached
    """
    call_number = 1
    response_results = results['value']
    max_calls_reached = False
    while True:
        if '@odata.nextLink' in results:
            req = web_helpers.make_api_call('GET', results['@odata.nextLink'], access_token)
            # pylint: disable=no-member
            if req.status_code == requests.codes.ok:
                r_json = req.json()
                response_results.extend(r_json['value'])
                results = r_json
                if call_number == max_calls:
                    max_calls_reached = True
                    break
                call_number = call_number + 1
            else:
                break
        else:
            break
    return response_results, max_calls_reached


def filter_events(events, onboarding, start_date, end_date, start_time, end_time, user_timezone=None):
    """
    Filter Events
    :param events: Events
    :param onboarding: Onboarding
    :param start_date: Start Date
    :param end_date: End Date
    :param start_time: Start Time
    :param end_time: End Time
    :param user_timezone: User local timezone
    :return: Events
    """

    if events is None:
        return None
    final_events = []

    # utc_day_start = get_datetime_from_date_time_tz(start_date, start_time, "UTC")
    # utc_day_end = get_datetime_from_date_time_tz(end_date, end_time, "UTC")
    utc_day_start = convert_datetime_timezone(datetime.combine(start_date, start_time), user_timezone, "UTC")
    utc_day_end = convert_datetime_timezone(datetime.combine(end_date, end_time), user_timezone, "UTC")

    for event in events['value']:
        add_event = not event['isCancelled']
        if add_event:
            if onboarding:
                utc_now = datetime.utcnow()
                utc_start = generic_functions.get_datetime_from_str(event['start']['dateTime'],
                                                                    event['start']['timeZone'])
                add_event = utc_now <= utc_start
            else:
                utc_event_start = generic_functions.get_datetime_from_str(event['start']['dateTime'],
                                                                          event['start']['timeZone'])
                utc_event_end = generic_functions.get_datetime_from_str(event['end']['dateTime'],
                                                                        event['end']['timeZone'])
                add_event = utc_event_start >= utc_day_start and utc_event_end <= utc_day_end

        if add_event:
            final_events.append(event)
    events['value'] = final_events
    return events


def get_user_data(alt_id, config):
    """
    Get user data
    :param alt_id: Altid
    :param config: Config
    :return: user data
    """
    user_data = mongo_helpers.get_user_details_by_alt_id(alt_id, config)
    if user_data is not None:
        access_token_expires_on = user_data["updated_on"] + timedelta(seconds=user_data["expires_in"])
        access_token_threshold = access_token_expires_on - timedelta(seconds=config.ACCESS_TOKEN_TIMEOUT_THRESHOLD)
        if datetime.now() > access_token_threshold:
            new_tokens = refresh_access_token(user_data['refresh_token'], config, user_data['scopes'])
            if new_tokens is not None:
                update_tokens = mongo_helpers.update_user_token_details(alt_id, new_tokens, config)
                if update_tokens is not None:
                    user_data.update(update_tokens)
                else:
                    return None
            else:
                # Handle the mechanism to send URL notification to user again
                # Need to handle specific error codes
                return None
    return user_data


def pre_process_calendar_notification(calendar_id, alt_id, config):
    """
    Pre Process Calendar Notification
    :param calendar_id: Calendar id
    :param alt_id: Alt id
    :param config: Config
    :return: 0 if too late, 1 if on time and > 1 if in furture (more than 300 secs i.e. 5 min)
    """
    user_data = get_user_data(alt_id, config)
    if user_data is None:
        print "pre_process_calendar_notification::DEBUG: User data is none"
        return 0

    event = get_event(calendar_id, user_data, config)
    if event is None or 'subject' not in event or event['isCancelled']:
        print "pre_process_calendar_notification::DEBUG: event is None or 'subject' " \
              "not in event or event['isCancelled']"
        return 0

    reminder_secs_before_start = (event['reminderMinutesBeforeStart']
                                  if event['reminderMinutesBeforeStart'] > 0
                                  else config.DEFAULT_CALENDAR_NOTIFICATION_MINUTES_AGO) * 60

    event_status = is_event_yet_to_start(event)

    if event_status["diff"] > (config.CALENDAR_EVENT_TOO_EARLY_THRESHOLD_SEC + reminder_secs_before_start):
        print "pre_process_calendar_notification::DEBUG: diff is in future (deferred meeting?): " + str(
            event_status["diff"])

        print "(config.CALENDAR_EVENT_TOO_EARLY_THRESHOLD_SEC + reminder_secs_before_start): " + str(
            config.CALENDAR_EVENT_TOO_EARLY_THRESHOLD_SEC + reminder_secs_before_start)

        event_status["diff"] = event_status["diff"] - reminder_secs_before_start

        print "New event_status[diff]: " + str(event_status["diff"])

        return int(event_status["diff"])

    return 1 if event_status["will_start"] else 0


def is_event_yet_to_start(event, start_time=True):
    """
    Check is event yet to start or not
    :param event: Events
    :param start_time: Start Time
    :return: will_start depends on Event yet to start or not and the time difference
    """
    if start_time:
        utc_start = generic_functions.get_datetime_from_str(event['start']['dateTime'], event['start']['timeZone'])
    else:
        utc_start = generic_functions.get_datetime_from_str(event['end']['dateTime'], event['end']['timeZone'])

    utc_now = datetime.utcnow()
    return {'will_start': utc_now <= utc_start, 'diff': (utc_start - utc_now).total_seconds()}


def register_email_created_webhook(user_info, config):
    response = None
    expiration_date_time = (datetime.now() + timedelta(hours=48)).strftime("%Y-%m-%dT%H:%M:%S.0000000Z")

    o365_inbox_webhook_payload = {
        "changeType": "created",
        "notificationUrl": config.o365_EMAIL_WEBHOOK_CREATED_ENDPOINT,
        "resource": "/me/mailfolders('inbox')/messages",
        "expirationDateTime": expiration_date_time,
        "clientState": "{}|{}".format(user_info["email"], user_info["alt_id"])
        # TODO: We need to use this sometime later
    }

    print("Webhook payload", json.dumps(o365_inbox_webhook_payload, default=str, indent=4))

    headers = {'User-Agent': 'python_agent/1.0',
               'Authorization': 'Bearer {0}'.format(user_info["access_token"]),
               'Accept': 'application/json'}

    request_id = str(uuid.uuid4())
    instrumentation = {'client-request-id': request_id,
                       'return-client-request-id': 'true'}

    headers.update(instrumentation)
    headers.update({'Content-Type': 'application/json'})

    registered = False
    try:
        print("Old subscription")
        if "o365_email_created_subscription_id" in user_info:
            api_response = requests.patch(
                "https://graph.microsoft.com/v1.0/subscriptions/{}".format(
                    user_info["o365_email_created_subscription_id"]),
                headers=headers,
                data=json.dumps({"expirationDateTime": expiration_date_time}))
            response = api_response.json()
            print("PATCH response: {}".format(json.dumps(response)))
            if api_response.status_code == 200:
                print("Update Webhook", response["id"], response["creatorId"])
                registered = True
        else:
            print("Not Present")
    except:
        traceback.print_exc()

    if not registered:
        try:
            response = requests.post(config.MS_GRAPH_ENDPOINT.format('/subscriptions'),
                                     headers=headers,
                                     data=json.dumps(o365_inbox_webhook_payload)).json()
            print("Register webhook POST response: {}".format(json.dumps(response)))
        except:
            traceback.print_exc()

    return response


def get_email(email_id, user_details, config,
              select="$subject,$body,$toRecipients,$ccRecipients,$from,$sentDateTime"):
    """
    Get Event
    :param event_id: Event id
    :param user_details: User Details
    :param config: Config
    :param select: parameters to be selected from Dictionary returned by MS_Graph
    :return: Event
    """

    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/messages/' + str(email_id))

        if select == "all":
            query_parameters = None
        else:
            query_parameters = {
                '$select': select
            }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result
