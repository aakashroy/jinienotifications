"""Optimizations"""
import json


def json_dumps_minify(pydict):
    """
    Json dumps minify
    :param pydict:
    :return:
    """
    return json.dumps(pydict, separators=(',', ':'), default=unicode)
