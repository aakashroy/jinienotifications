"""Plugin Helpers"""

import importlib
import os
import traceback

from common import logging


class PluginClass(object):
    """Plugin Class"""
    PLUGIN_CACHE = None

    def get_plugins(self, plugin_dir, package_name, run_method, service_name_method):
        """
        Get Plugins
        :param plugin_dir: Plugin directory
        :param package_name: Package Name
        :param run_method: Run Method
        :param service_name_method: Service Name Method
        :return:
        """
        if PluginClass.PLUGIN_CACHE is None:
            # plugin_dir = "/home/aakash/work/projx/jinienotifications/plugins"
            # package_name = "plugins"
            # method_name = "run"
            module_names = []
            method_objs = []

            for root, dummy_dirnames, filenames in os.walk(plugin_dir):
                for filename in filenames:
                    try:
                        if filename == "plugin.py":
                            module_name = root.replace(plugin_dir, "").lstrip("/").replace("/", ".") \
                                          + "." + filename.replace(".py", "")
                            module_names.append(module_name)
                    except Exception:
                        logging.log(logging.SEVERITY_CRITICAL, "MAKE-MODULE-NAME",
                                    traceback.format_exc())

            for module_name in module_names:
                try:
                    module_obj = importlib.import_module(package_name + "." + module_name)
                    run_method_obj = getattr(module_obj, run_method)
                    service_name_method_obj = getattr(module_obj, service_name_method)
                    method_objs.append((run_method_obj, service_name_method_obj))
                except Exception:
                    logging.log(logging.SEVERITY_CRITICAL, "LOAD-MODULE-FUNCTION",
                                traceback.format_exc())

            PluginClass.PLUGIN_CACHE = method_objs

        return PluginClass.PLUGIN_CACHE


if __name__ == "__main__":
    PluginClass().get_plugins("/home/aakash/work/projx/jinienotifications/plugins",
                              "plugins",
                              "run",
                              "service_name")
