from common import mongo_helpers
from common.mongo_helpers import fetch_all_user_alt_ids
from simplex_communications import config
from simplex_communications.event_producers import scheduler


def send_motd():
    users_in_mongo_cursor = fetch_all_user_alt_ids(config)
    for mongo_user in users_in_mongo_cursor:
        user_data = mongo_helpers.get_user_details_by_alt_id(mongo_user['alt_id'], config)
        scheduler.schedule(mongo_user['alt_id'], user_data, user_data['allowed_services'], config, True, True)
        print "MOTD SENT TO: " + user_data['email']


if __name__ == "__main__":
    send_motd()
