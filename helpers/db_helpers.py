import time
import traceback
import string
import re
import MySQLdb
import pymongo as pymongo
import pymssql

from helpers import logging
from pymongo.errors import BulkWriteError

REATTEMPT_COUNT = 10

# MYSQL_CREDENTIALS = {"IP": "10.0.2.17", "PORT": '3306', "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355",
#                      "DB": "qtrain"}


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def clean_string(inputstr):
    outputstr = inputstr.lower()
    outputstr = translate_punctuation_to_empty(outputstr)
    outputstr = re.sub(r'\s+', ' ', outputstr)
    return outputstr.strip()

def fetch_org_config_by_id(orgId, conf):
    """
    Fetches org_mapping configs from Mongo
    :param conf: Configuration details
    :return:
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_ORG_MAPPING]
        result = coll.find({"org_id": orgId}, {"org_id": 1, "disallowed_domains": 1, "employment_status_ids": 1, "timezone": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result

def fetch_subscription_pending_users(orgId, conf):
    """
    Fetches org_mapping pending subscriptions users list from Mongo
    :param conf: Configuration details
    :param orgId: Configuration details
    :return:
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": orgId, "access_token":None}, {"email": 1, "alt_id": 1, "timezone": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def fetch_org_config(conf):
    """
    Fetches org_mapping configs from Mongo
    :param conf: Configuration details
    :return:
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_ORG_MAPPING]
        result = coll.find({}, {"org_id": 1, "disallowed_domains": 1, "employment_status_ids": 1, "timezone": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result



def fetch_all_user_alt_ids_by_worksite(conf, orgId, workSite):
    """
    Fetches all Users (only alt ids) from Mongo
    :param conf: Configuration details
    :return:
    """
    result = None

    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": orgId, "worksite": workSite}, {"alt_id": 1,"worksite": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def fetch_all_user_alt_ids(conf,orgId):
    """
    Fetches all Users (only alt ids) from Mongo
    :param conf: Configuration details
    :return:
    """
    result = None

    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": orgId}, {"alt_id": 1})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def insert_bulk_users(conf, users_list):
    """
    Insert a bulk user into Mongo DB
    :param conf: Configuration details
    :param users_list: list of users
    :return:
    """
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.insert_many([{
            "emp_id": user["emp_id"],
            "org_id":user["org_id"],
            "email": user["email"],
            "official_email": user["official_email"],
            "alt_id": user["alt_id"],
            "allowed_services": {
                "enabled": True,
                "motd": {
                    "enabled": True,
                    "schedule": "0 8 * * *"
                }
            },
            "first_name": user["first_name"] if user["first_name"] else "",
            "middle_name": user["middle_name"] if user["middle_name"] else "",
            "last_name": user["last_name"] if user["last_name"] else "",
            "search_email": user["search_email"].lower(),
            "search_f_name": clean_string(user["search_f_name"]),
            "search_fm_name": clean_string(user["search_fm_name"]),
            "search_fl_name": clean_string(user["search_fl_name"]),
            "search_fml_name": clean_string(user["search_fml_name"]),
            "timezone": user["timezone"]
        } for user in users_list])
        if result:
            return True
    except BulkWriteError as bwe:
        # print str(traceback.format_exc())
        print("Printing bwe details")
        print(bwe.details)
        print("Printing details")
        print(bwe.details['writeErrors'])

    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def insert_new_user(conf, alt_id, emp_id, email, first_name, middle_name, last_name):
    """
    Insert a new user into Mongo DB
    :param conf: Configuration details
    :param alt_id: alt id (user id)
    :param emp_id: employee id
    :param email: email address
    :return:
    """

    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        first_name_val = first_name if first_name else "",
        middle_name_val = middle_name if middle_name else "",
        last_name_val = last_name if last_name else "",

        result = coll.insert_one({
            "emp_id": emp_id,
            "email": email,
            "alt_id": alt_id,
            "allowed_services": {
                "enabled": True,
                "motd": {
                    "enabled": True,
                    "schedule": "0 8 * * *"
                }
            },
            "first_name": first_name_val,
            "middle_name": middle_name_val,
            "last_name": last_name_val,
            "search_email": email.lower(),
            "search_f_name": clean_string(first_name_val),
            "search_fm_name": clean_string(first_name_val + middle_name_val),
            "search_fl_name": clean_string(first_name_val + last_name_val),
            "search_fml_name": clean_string(first_name_val + middle_name_val + last_name_val)
        })
        if result:
            return True
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def delete_bulk_users(conf, alt_id_list):
    """
    Delete a users list from Mongo DB
    :param conf: Configuration details
    :param alt_id_list: alt ids list (user id)
    :return:
    """
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.delete_many({"alt_id": {'$in': alt_id_list} })
        if result:
            return True
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def delete_user(conf, alt_id):
    """
    Delete a user from Mongo DB
    :param conf: Configuration details
    :param alt_id: alt id (user id)
    :return:
    """
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.delete_many({"alt_id": alt_id})
        if result:
            return True
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return False


def connect_to_sql_server(conf):
    """
    Connect to MS-SQL server
    :return: Connection object
    """
    start = time.time()
    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = pymssql.connect(conf.SQL_CREDENTIALS["IP"], conf.SQL_CREDENTIALS["USER"],
                                                    conf.SQL_CREDENTIALS["PASSWORD"], conf.SQL_CREDENTIALS["DB"])
            crsr = sql_connection_client.cursor()
            connected_boolean = True
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Connection Successful")
        except Exception:
            logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", traceback.format_exc())
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Reconnect attempt : {0}".format(attempts))
    if connected_boolean is False:
        logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", "Failed connecting to SQL Server. Check Error Log")
    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Took: " + str(end - start) + " seconds.")
    return crsr


def connect_to_mysql_server(conf):
    """
    Connect to MY-SQL Server
    :return: Connection Object
    """

    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = MySQLdb.connect(conf.MYSQL_CREDENTIALS["IP"], conf.MYSQL_CREDENTIALS["USER"],
                                                    conf.MYSQL_CREDENTIALS["PASSWORD"], conf.MYSQL_CREDENTIALS["DB"])
            crsr = sql_connection_client.cursor()
            connected_boolean = True
            # print("MYSQL Server connection successful")
            # addToLogFile("SQL Server connection successful")
        except Exception as ex:
            print "Error:", ex.message
            # addtoErrorFile("SQL Server Connection Failed : {0}".format(ex.message))
            attempts += 1
            print "Reconnect attempt : {0}".format(attempts)

    if connected_boolean is False:
        print "Failed connecting to SQL Server. Check Error Log"
    else:
        return crsr


if __name__ == "__main__":
    print "connected to mysql: ", connect_to_mysql_server(config)
