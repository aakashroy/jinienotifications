"""Logging"""
import inspect
import json

from datetime import datetime

SEVERITY_DEBUG = "DEBUG"
SEVERITY_ERROR = "ERROR"
SEVERITY_CRITICAL = "CRITICAL"


def safe_print(to_print):
    """
    Function to safely print statements without crashing the app
    :param to_print: The string to print
    :return: None
    """
    if isinstance(to_print, unicode):
        to_print = to_print.encode("utf8")
    print to_print


def log(severity, key, message, variable_dump=None):
    """
    Log Function
    :param severity: Severity
    :param key: Key
    :param message: Message
    :param variable_dump: Variable Dump
    :return: Logs
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    previous_frame = inspect.currentframe().f_back
    if previous_frame is not None:
        (filename, dummy_line_number, function_name, dummy_lines, dummy_index) = inspect.getframeinfo(previous_frame)
    else:
        filename = "NA"
        function_name = "NA"
    safe_print(severity + " | " + key + " | " + log_time + " | " + filename.split("/")[
        -1] + " | " + function_name + " | " + message)
    if variable_dump is not None:
        print "---vardump---"
        if isinstance(variable_dump, dict):
            # variable_dump = copy.deepcopy(variable_dump)
            # variable_dump.pop("access_token", "")
            # variable_dump.pop("refresh_token", "")
            # variable_dump.pop("id_token", "")
            safe_print(json.dumps(variable_dump, indent=4, default=unicode))
        else:
            print "Warning: Please send a dict and avoid sending any other data type in vardump!"
            safe_print(variable_dump)
        print "-------------"


def check():
    """
    Check Success
    :return:
    """
    log(SEVERITY_DEBUG, "USER-ONBOARDING", "Success",
        {"name": "Hello", "mail": "World"})


if __name__ == "__main__":
    check()
