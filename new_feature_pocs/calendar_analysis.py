"""This module checks tokens exists in MongoDB"""
import traceback
from datetime import timedelta, datetime, date
from time import time, sleep

import pymongo
import requests
import pandas as pd

from common import mongo_helpers, web_helpers
from common.o365_helpers import refresh_access_token, get_more_data, pad_datetime_str_for_o365_api


def get_user_details_by_alt_id_and_token(alt_id, conf):
    """
    Get user details from alt id
    :param alt_id: Altid
    :param conf: Config
    :return: User details
    """
    result = None

    try:
        alt_id = int(alt_id)
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one({"alt_id": alt_id, "access_token": {"$exists": True}})
    except Exception:
        traceback.print_exc()

    return result


def get_user_data(alt_id, config):
    """
    Get user data
    :param alt_id: Altid
    :param config: Config
    :return: user data
    """
    user_data = get_user_details_by_alt_id_and_token(alt_id, config)
    if user_data is not None:
        access_token_expires_on = user_data["updated_on"] + timedelta(seconds=user_data["expires_in"])
        access_token_threshold = access_token_expires_on - timedelta(seconds=config.ACCESS_TOKEN_TIMEOUT_THRESHOLD)
        if datetime.now() > access_token_threshold:
            new_tokens = refresh_access_token(user_data['refresh_token'], config, user_data["scopes"])
            if new_tokens is not None:
                update_tokens = mongo_helpers.update_user_token_details(alt_id, new_tokens, config)
                if update_tokens is not None:
                    user_data.update(update_tokens)
                else:
                    return None
            else:
                # Handle the mechanism to send URL notification to user again
                # Need to handle specific error codes
                return None
    return user_data


def get_events(user_details, start_date, end_date, start_time, end_time, config):
    """
    Get Events
    :param user_details: User Details
    :param start_date: Start DateE
    :param end_date: End Date
    :param start_time: Start Time
    :param end_time: End Time
    :param config: Config
    :return: Events
    """
    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/calendar/calendarView')

        query_parameters = {
            'startDateTime': pad_datetime_str_for_o365_api(start_date, start_time, "UTC"),
            'endDateTime': pad_datetime_str_for_o365_api(end_date, end_time, "UTC"),
            # Is this end time inclusive?
            '$orderby': 'start/dateTime ASC',
            '$select': 'start,end,subject,location,type,attendees,isCancelled,isOrganizer,webLink,onlineMeetingUrl,isAllDay,reminderMinutesBeforeStart'
        }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)
        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
            result['value'], dummy_max_calls_reached = get_more_data(result, user_details['access_token'], 1000)
        else:
            raise IOError("o365 Server refused to provide data!!")
    except Exception:
        print ("IOERROR")
        traceback.print_exc()

    return result


def get_sent_emails(user_details, start_date, end_date, config):
    """
    Get unread emails
    :param user_details: User details
    :param start_date: Start Date
    :param end_date: End Date
    :param config: Config
    :return: Unread Mails
    """
    result = None
    try:
        get_messages_url = config.MS_GRAPH_ENDPOINT.format('/me/mailFolders(\'SentItems\')/messages')

        # query_parameters = {'$top': '1000',
        #                     '$filter': ' receivedDateTime ge ' + str(start_date) +
        #                                ' and receivedDateTime lt ' + str(end_date) +
        #                                ' and isRead eq true ' +
        #                                ' and inferenceClassification eq \'focused\'',
        #                     '$select': 'importance, from, toRecipients, ccRecipients, bccRecipients, sender, importance, sentDateTime, lastModifiedDateTime, receivedDateTime, createdDateTime'}

        query_parameters = {'$top': '1000',
                            '$filter': ' sentDateTime ge ' + str(start_date) +
                                       ' and sentDateTime lt ' + str(end_date),
                            '$select': 'importance, from, toRecipients, ccRecipients, bccRecipients, sender, importance, sentDateTime, lastModifiedDateTime, receivedDateTime, createdDateTime'}

        req = web_helpers.make_api_call('GET', get_messages_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
            result['value'], max_calls_reached = get_more_data(result, user_details['access_token'], 1000)
            if max_calls_reached:
                result['max_calls_reached'] = max_calls_reached
        else:
            raise IOError("o365 Server refused to provide data!!")
    except Exception:
        traceback.print_exc()
    return result


def convert_to_datetime(datetime_string):
    '''
    :param datetime_string: timestamp string
    :return: datetime object
    '''
    try:
        created_datetime = datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S.%f")
    except:
        created_datetime = datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%SZ")
    return created_datetime


def mail_contains_ps_recipients(mail_recipients_list):
    for recipient in mail_recipients_list:
        if 'peoplestrong.com' in recipient['emailAddress']['address']:
            return True
    return False


if __name__ == "__main__":
    from simplex_communications import config

    # target_user_ids = [2060312, 1077498, 1123557, 118290, 119571, 135751, 1519005, 1621491, 1731841, 1735873, 1802800,
    #                    181235, 181499, 1823873, 1838538, 1864328, 1864329, 1864331, 1864333, 1864335, 1864342, 1864343,
    #                    1988535, 1989459, 2016706, 2138662, 2147523, 2148468, 2149288, 2360186, 2458254, 2468840,
    #                    2483972, 2526151, 2529877, 2530427, 2543582, 262471, 2651146, 2651147, 2740403, 2756852, 2756904,
    #                    276469, 2804078, 28295, 28298, 28302, 28306, 2835503, 28364, 28370, 2837873, 28398, 28399, 28400,
    #                    28401, 28402, 28403, 28404, 28405, 2878936, 2878966, 2892506, 290669, 299147, 308966, 3146912,
    #                    3146937, 3192102, 3209185, 321567, 321568, 321798, 3223901, 3225199, 3233374, 3248527, 327404,
    #                    3283537, 3288706, 3310021, 3351904, 336994, 357579, 58889, 65426, 65842, 67294, 67679, 681285,
    #                    83341, 84838]
    start = time()

    target_user_ids = [2360186,
                       2756852,
                       2756904,
                       2837873,
                       2892506,
                       3192102,
                       3209185,
                       321567,
                       3225199,
                       336994,
                       67679,
                       83341
                       ]

    # target_user_ids = [2060312]
    # users_to_analyse = list()
    # for target_user_id in target_user_ids:
    #     print "Checking: " + str(target_user_id)
    #     target_user = get_user_data(target_user_id, config)
    #     if target_user:
    #         users_to_analyse.append(target_user)

    summary_data_list = []
    month_pos_name_dict = {1: 'Jan',
                           2: 'Feb',
                           3: 'Mar',
                           4: 'Apr',
                           5: 'May',
                           6: 'Jun',
                           7: 'Jul',
                           8: 'Aug',
                           9: 'Sep',
                           10: 'Oct',
                           11: 'Nov',
                           12: 'Dec'}

    for user_id in target_user_ids:
        target_user = get_user_data(user_id, config)
        if target_user:
            email_of_user = target_user['search_email']
            end_date = datetime.now().date()
            # start_date = end_date - timedelta(days=30)
            start_date = date(2018, 10, 1)
            start_time = datetime.strptime('00:00:00.000000', '%H:%M:%S.%f').time()
            end_time = datetime.strptime('23:59:59.000000', '%H:%M:%S.%f').time()
            sleep(1)
            emails_to_analyze = get_events(target_user, start_date, end_date, start_time, end_time, config)

            month_wise_email_count_dict = {'email_address': email_of_user,
                                           'user_id': target_user['alt_id']}
            # print(len(emails_to_analyze['value']))
            if emails_to_analyze:
                # print(emails_to_analyze['value'][0]['toRecipients'])
                for mail in emails_to_analyze['value']:
                    if mail['sender']['emailAddress']['address'] == target_user[
                        'search_email']:  # mail sent by same user
                        if mail_contains_ps_recipients(mail['toRecipients']):  # list of dicts
                            mail_send_time_dt = convert_to_datetime(mail['sentDateTime'])
                            mail_send_year = mail_send_time_dt.year
                            mail_send_month = mail_send_time_dt.month
                            if mail_send_month < 10:
                                send_key = str(mail_send_year) + "_0" + str(mail_send_month)
                            else:
                                send_key = str(mail_send_year) + "_" + str(mail_send_month)
                            month_wise_email_count_dict[send_key] = month_wise_email_count_dict.get(send_key, 0) + 1
                        else:
                            print('this mail does not consists of ps recipients.. not counting this mail')
                            print(mail['toRecipients'])
                            print('--------')
                    else:
                        print("this mail is not sent by this user !!!!")
                        print(mail['sender']['emailAddress']['address'], target_user['search_email'])
                        print('------------')
                        # exit(1)
                summary_data_list.append(month_wise_email_count_dict)

    # outputing to csv
    summary_data_df = pd.DataFrame(summary_data_list)
    month_column = []
    for col in summary_data_df.columns:
        if col not in ['email_address', 'user_id']:
            month_column.append(col)
    month_column = sorted(month_column)  # getting month columns and sorting them
    # print(month_column)
    new_month_column = []
    for month in month_column:
        x = month.split('_')
        new_month_column.append(str(x[0]) + ' ' + month_pos_name_dict[int(x[1])])

    column_order = ['user_id', 'email_address'] + month_column
    summary_data_df = summary_data_df[column_order]
    summary_data_df.columns = ['user_id', 'email_address'] + new_month_column
    summary_data_df.to_csv("summary_data_df_0422_rem.csv", encoding='utf-8', index=False)
    print("total time", time() - start)
