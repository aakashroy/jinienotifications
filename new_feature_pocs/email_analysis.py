"""This module checks tokens exists in MongoDB"""
import traceback
from datetime import timedelta, datetime

import pymongo
import requests

from common import mongo_helpers, web_helpers
from common.o365_helpers import refresh_access_token, get_more_data


def get_user_details_by_alt_id_and_token(alt_id, conf):
    """
    Get user details from alt id
    :param alt_id: Altid
    :param conf: Config
    :return: User details
    """
    result = None

    try:
        alt_id = int(alt_id)
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one({"alt_id": alt_id, "access_token": {"$exists": True}})
    except Exception:
        traceback.print_exc()

    return result


def get_user_data(alt_id, config):
    """
    Get user data
    :param alt_id: Altid
    :param config: Config
    :return: user data
    """
    user_data = get_user_details_by_alt_id_and_token(alt_id, config)
    if user_data is not None:
        access_token_expires_on = user_data["updated_on"] + timedelta(seconds=user_data["expires_in"])
        access_token_threshold = access_token_expires_on - timedelta(seconds=config.ACCESS_TOKEN_TIMEOUT_THRESHOLD)
        if datetime.now() > access_token_threshold:
            new_tokens = refresh_access_token(user_data['refresh_token'], config, user_data["scopes"])
            if new_tokens is not None:
                update_tokens = mongo_helpers.update_user_token_details(alt_id, new_tokens, config)
                if update_tokens is not None:
                    user_data.update(update_tokens)
                else:
                    return None
            else:
                # Handle the mechanism to send URL notification to user again
                # Need to handle specific error codes
                return None
    return user_data


def get_emails(user_details, start_date, end_date, config):
    """
    Get unread emails
    :param user_details: User details
    :param start_date: Start Date
    :param end_date: End Date
    :param config: Config
    :return: Unread Mails
    """
    result = None
    try:
        get_messages_url = config.MS_GRAPH_ENDPOINT.format('/me/mailFolders(\'SentItems\')/messages')

        query_parameters = {'$top': '1000',
                            '$filter': ' receivedDateTime ge ' + str(start_date) +
                                       ' and receivedDateTime lt ' + str(end_date) +
                                       ' and isRead eq true ' +
                                       ' and inferenceClassification eq \'focused\'',
                            '$select': 'importance, from, toRecipients, ccRecipients, bccRecipients, sender, importance, sentDateTime, lastModifiedDateTime, receivedDateTime, createdDateTime'}

        req = web_helpers.make_api_call('GET', get_messages_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
            result['value'], max_calls_reached = get_more_data(result, user_details['access_token'], 1000)
            if max_calls_reached:
                result['max_calls_reached'] = max_calls_reached
    except Exception:
        traceback.print_exc()
    return result


if __name__ == "__main__":
    from simplex_communications import config

    target_user_ids = [
        2060312]  # , 1077498, 1123557, 118290, 119571, 135751, 1519005, 1621491, 1731841, 1735873, 1802800,
    #                    181235, 181499, 1823873, 1838538, 1864328, 1864329, 1864331, 1864333, 1864335, 1864342, 1864343,
    #                    1988535, 1989459, 2016706, 2138662, 2147523, 2148468, 2149288, 2360186, 2458254, 2468840,
    #                    2483972, 2526151, 2529877, 2530427, 2543582, 262471, 2651146, 2651147, 2740403, 2756852, 2756904,
    #                    276469, 2804078, 28295, 28298, 28302, 28306, 2835503, 28364, 28370, 2837873, 28398, 28399, 28400,
    #                    28401, 28402, 28403, 28404, 28405, 2878936, 2878966, 2892506, 290669, 299147, 308966, 3146912,
    #                    3146937, 3192102, 3209185, 321567, 321568, 321798, 3223901, 3225199, 3233374, 3248527, 327404,
    #                    3283537, 3288706, 3310021, 3351904, 336994, 357579, 58889, 65426, 65842, 67294, 67679, 681285,
    #                    83341, 84838]
    users_to_analyse = list()
    for target_user_id in target_user_ids:
        print "Checking: " + str(target_user_id)
        target_user = get_user_data(target_user_id, config)
        if target_user:
            users_to_analyse.append(target_user)

    for user in users_to_analyse:
        target_user = get_user_data(user['alt_id'], config)
        email_of_user = user['search_email']
        end_date = datetime.utcnow().date()
        start_date = end_date - timedelta(days=30)
        emails_to_analyze = get_emails(target_user, start_date, end_date, config)
