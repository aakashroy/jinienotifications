import json

import requests

from common import o365_helpers, web_helpers
from simplex_communications import config

user_data = o365_helpers.get_user_data(2205395, config)

fmt_url = config.MS_GRAPH_ENDPOINT.format('/me/findMeetingTimes')

query_parameters = {
    'maxCandidates': 2,
    "attendees": [
        {
            "type": "required",
            "emailAddress": {
                "address": "harsimran.walia@peoplestrong.com"
            }
        },
        {
            "type": "required",
            "emailAddress": {
                "address": "nishant.garg@peoplestrong.com"
            }
        },
        {
            "type": "required",
            "emailAddress": {
                "address": "nishant.garg@peoplestrong.com"
            }
        }
    ],
    "timeConstraint": {
        "activityDomain": "work",
        "timeslots": [
            {
                "start": {
                    "dateTime": "2019-04-12T09:30:00",
                    "timeZone": "India Standard Time"
                },
                "end": {
                    "dateTime": "2019-04-13T12:30:00",
                    "timeZone": "India Standard Time"
                }
            }
        ]
    }
}

req = web_helpers.make_api_call('POST', fmt_url, user_data['access_token'],
                                payload=query_parameters)

if req.status_code == requests.codes.ok:
    print json.dumps(req.json(), indent=4)
