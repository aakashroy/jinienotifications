import sync_users
from simplex_communications import config as configuration


def onboard_sync_user(message_dict):
    """

    :param message_data: message_data
    :return:
    """
    org_id = int(message_dict['org_id'])
    sync_users.sync(org_id, configuration)

if __name__ == "__main__":
    #print timeZone
    message = dict()
    message["org_id"] =19
    onboard_sync_user(message)