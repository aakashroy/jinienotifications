"""This module syncs users from Talentpact ATS DB to MongoDB"""
import socket
import traceback
import pytz
from datetime import datetime
from tzlocal import get_localzone

from simplex_communications import config as configuration

import ignore_users as ignore_users
from helpers import email_functions
from helpers.db_helpers import connect_to_sql_server
from helpers.db_helpers import fetch_all_user_alt_ids,fetch_org_config_by_id, fetch_all_user_alt_ids_by_worksite, insert_bulk_users,delete_bulk_users,fetch_org_config, delete_user

QUERY = "select hre.EmployeeID, hre.OfficialEmailAddress, hre.UserId, su.FirstName, su.MiddleName, su.LastName \
         from dbo.HrEmployee as hre \
         inner join dbo.SysUser as su on su.UserID=hre.UserID \
         where hre.OrganizationId=%s and hre.EmploymentStatusID in %s \
         and hre.OfficialEmailAddress is not NULL"

timeZone = str(get_localzone())


def local_logger(old_log_string, new_log_string):
    """

    A local logger for collating logs and sending as email.

    :param old_log_string: The log string already saved so far
    :param new_log_string: The new string to append
    :return: Updated log string
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return old_log_string + u"<br/>" + log_time + u" | " + new_log_string


def check_emails(user, disallowed_domains_list):
    try:
        user_email_split = user["email"].split('@')
        if len(user_email_split) == 2:
            user_email_domain = user_email_split[1]
            if any(user_email_domain == disallowed_domain for disallowed_domain in disallowed_domains_list):
                user["reason"] = "invalid email domain"
        else:
            user["reason"] = "invalid email"
        return user
    except Exception:
        user["reason"] = "invalid email"
        return user


def work_site_sync(config, org_id, status_list, disallowed_domains, timeZone):
    email_log = ""
    email_log = local_logger(email_log, "Now syncing orgid: " + str(org_id))
    try:
        sql_con = connect_to_sql_server(config)
        email_log = local_logger(email_log, "Connected to TalentPact DB")
        sql_con.execute(QUERY, (org_id, status_list))
        email_log = local_logger(email_log, "Query to fetch users executed")
        users_to_sync = sql_con.fetchall()
        email_log = local_logger(email_log, str(len(users_to_sync)) + " users fetched from TalentPact DB")
        users_in_sql = set([x[2] for x in users_to_sync])
        users_in_sql_lookup_dict = {z: (x, y, z, f, m, l) for (x, y, z, f, m, l) in users_to_sync}
        users_in_mongo_cursor = fetch_all_user_alt_ids(config, org_id)
        users_in_mongo = set()
        for mongo_user in users_in_mongo_cursor:
            users_in_mongo.add(mongo_user['alt_id'])
        email_log = local_logger(email_log, str(len(users_in_mongo)) + " users fetched from Mongo DB")
        users_to_add = users_in_sql.difference(users_in_mongo)
        email_log = local_logger(email_log, str(len(users_to_add)) + " users to add")
        users_to_del = users_in_mongo.difference(users_in_sql)
        email_log = local_logger(email_log, str(len(users_to_del)) + " users to remove")
        email_log = local_logger(email_log, "Deleting users . . .")
        deleted_user_success_count = 0
        deleted_user_error_count = 0
        users_to_del = list(users_to_del)
        ignore_users_list = set([ ignore_item['alt_id'] for ignore_item in ignore_users.IGNORE_EMAILS])
        users_to_add = users_to_add.difference(ignore_users_list)
        if len(users_to_del) == 0:
            email_log = local_logger(email_log, "zero records found to delete")
        elif delete_bulk_users(config, users_to_del):
            deleted_user_success_count = len(users_to_del)
        else:
            deleted_user_error_count = deleted_user_error_count + 1

        email_log = local_logger(email_log, "Adding users . . .")
        inserted_user_success_count = 0
        inserted_user_error_count = 0
        add_users_list = []
        invalid_users = []
        for user_id in users_to_add:
            first_name = users_in_sql_lookup_dict[user_id][3] if users_in_sql_lookup_dict[user_id][3] else ""
            middle_name = users_in_sql_lookup_dict[user_id][4] if users_in_sql_lookup_dict[user_id][4] else ""
            last_name = users_in_sql_lookup_dict[user_id][5] if users_in_sql_lookup_dict[user_id][5] else ""
            email = str(users_in_sql_lookup_dict[user_id][1].strip()).lower()
            #user_worksite = users_in_sql_lookup_dict[user_id][6] if users_in_sql_lookup_dict[user_id][6] else worksite
            userTimezone = timeZone
            # userTimezone = timeZone
            user_data = {
                "emp_id": users_in_sql_lookup_dict[user_id][0],
                "org_id": org_id,
                "email": email,
                "official_email": email,
                "alt_id": users_in_sql_lookup_dict[user_id][2],
                "first_name": first_name,
                "middle_name": middle_name,
                "last_name": last_name,
                "search_email": email,
                "search_f_name": first_name,
                "search_fm_name": first_name + middle_name,
                "search_fl_name": first_name + last_name,
                "search_fml_name": first_name + middle_name + last_name,
                "timezone": userTimezone
            }
            invalid_emails_dict = check_emails(user_data, disallowed_domains)
            if invalid_emails_dict.has_key("reason"):
                invalid_users.append(user_data)
            else:
                add_users_list.append(user_data)
        email_log = local_logger(email_log, str(len(invalid_users)) + " invalid emails found")
        if len(add_users_list) == 0:
            email_log = local_logger(email_log, "already sync databases")
        elif insert_bulk_users(config, add_users_list):
            inserted_user_success_count = len(add_users_list)
            email_log = local_logger(email_log, "successfully inserted bulk users list")
        else:
            inserted_user_error_count = inserted_user_error_count + 1
            email_log = local_logger(email_log, "failure while inserting bulk users list")
        invalid_users_list_count = len(invalid_users)
        email_log = local_logger(email_log, str(invalid_users_list_count) + " invalid users data count")
        email_log = local_logger(email_log, str(inserted_user_success_count) + " users added")
        email_log = local_logger(email_log, str(inserted_user_error_count) + " errors while adding users")
        email_log = local_logger(email_log, str(deleted_user_success_count) + " users removed")
        email_log = local_logger(email_log, str(deleted_user_error_count) + " errors while removing users")

    except Exception:
        email_log = local_logger(email_log, traceback.format_exc())
    print email_log
    print email_functions.send_mail("altml@alt.peoplestrong.com", "Alt Jinie Notification System", [{
        'recipient_email': 'gorla.edukondalu@peoplestrong.com',
        'subject': 'Activity Log  | Jinie Notifications User Sync | ' + socket.gethostname(),
        'body': 'Hi, Activity log is below: <hr/>' + email_log + "<hr/><br/>Best,<br/>Jinie | I'm the nu cool!"
    }])


def sync(orgId, config):
    """

    :param config: Configuration
    :return:
    """
    config_in_mongo_cursor = fetch_org_config_by_id(orgId, config)

    for configRecord in config_in_mongo_cursor:
        org_id = int(configRecord["org_id"])
        status_list = configRecord["employment_status_ids"]
        disallowed_domains = configRecord["disallowed_domains"]
        defaultTimezone = configRecord.get("timezone") if configRecord.get("timezone") else timeZone
        work_site_sync(config, org_id, status_list, disallowed_domains, defaultTimezone)


def convert_datetime_timezone(dt, source_tz, dest_tz):
    tz_src = pytz.timezone(source_tz)
    tz_dest = pytz.timezone(dest_tz)
    dt = dt.replace(tzinfo=None)
    dt = tz_src.localize(dt)
    dt = dt.astimezone(tz_dest)
    return dt.replace(tzinfo=None)


if __name__ == "__main__":
    #print timeZone
    sync(19, configuration)