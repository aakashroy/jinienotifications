"""Connection to TalentPact and Qtrain Database"""
import pymssql
import time
import traceback

import MySQLdb

from common import logging

REATTEMPT_COUNT = 10

SQL_CREDENTIALS = {"IP": "10.226.0.187", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867",
                   "DB": "TalentPact"}  ## Prod

# SQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "AltMApp", "PASSWORD": "H0n35TL1F31SGr@t3",
#                    "DB": "TalentPact"}  ## Prod

MYSQL_CREDENTIALS = {"IP": "13.71.27.119", "PORT": "3306", "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355",
                     "DB": "qtrain"}


# MYSQL_CREDENTIALS = {"IP": "10.0.2.17", "PORT": '3306', "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355",
#                      "DB": "qtrain"}


def connect_to_sql_server():
    """
    Connect to MS-SQL server
    :return: Connection object
    """
    start = time.time()
    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = pymssql.connect(SQL_CREDENTIALS["IP"], SQL_CREDENTIALS["USER"],
                                                    SQL_CREDENTIALS["PASSWORD"], SQL_CREDENTIALS["DB"])
            crsr = sql_connection_client.cursor()
            connected_boolean = True
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Connection Successful")
        except Exception:
            logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", traceback.format_exc())
            attempts += 1
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Reconnect attempt : {0}".format(attempts))
            time.sleep(5)
    if connected_boolean is False:
        logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", "Failed connecting to SQL Server. Check Error Log")
    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Took: " + str(end - start) + " seconds.")
    return crsr


def connect_to_mysql_server():
    """
    Connect to MY-SQL Server
    :return: Connection Object
    """

    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = MySQLdb.connect(MYSQL_CREDENTIALS["IP"], MYSQL_CREDENTIALS["USER"],
                                                    MYSQL_CREDENTIALS["PASSWORD"], MYSQL_CREDENTIALS["DB"])
            crsr = sql_connection_client.cursor()
            connected_boolean = True
            # print("MYSQL Server connection successful")
            # addToLogFile("SQL Server connection successful")
        except Exception as ex:
            print "Error:", ex.message
            # addtoErrorFile("SQL Server Connection Failed : {0}".format(ex.message))
            attempts += 1
            print "Reconnect attempt : {0}".format(attempts)

    if connected_boolean is False:
        print "Failed connecting to SQL Server. Check Error Log"
    else:
        return crsr


if __name__ == "__main__":
    print "connected to mysql: ", connect_to_mysql_server()
