"""DB queries for course recommender"""


def fetch_empcode_tentantid(userid, tpconn):  # talentpact
    """
    Fetches employee code and tenant id
    :param userid: Userid
    :param tpconn: Connection to Talentpact Database
    :return: EmployeeCode and Tenantid
    """
    output = {"success": False}
    query = "Select EmployeeCode,TenantId from HrEmployee where userid=%s"
    tpconn.execute(query, userid)
    result = list(tpconn.fetchall())
    if result:
        output = {"success": True, "emp_code": result[0][0], "tenant_id": result[0][1]}
    return output


def fetch_userid_enterpriseid(tenantid, employeecode, qtconn):  # qtrain
    """
    Fetches userid and enterprise id
    :param tenantid: Tenantid
    :param employeecode: EmployeeCode
    :param qtconn: Connection to Qtrain Database
    :return: Autoid and Enterpriseid
    """
    output = {"success": False}
    query = "SELECT autoid,enterpriseID from tbl_users usr inner join enterprise e on usr.enterpriseID=e.eid " \
            "where ps_tenant_id=%s and client_userid=%s"
    qtconn.execute(query, (tenantid, employeecode))
    result = list(qtconn.fetchall())
    if result:
        output = {"success": True, "user_id": result[0][0], "enterprise_id": result[0][1]}
    return output


def get_course_names(ids, qtconn):  # qtrain
    """
    Finds course name using Employee ids
    :param ids: Comma separated Courseids as string
    :param qtconn: Connection to Qtrain Database
    :return: Courseid,Coursename,Number of videos,no of assesents
    """
    query = "SELECT tbl.id, tbl.courseName,tbl_lbases.no_of_videos,tbl_lbases.no_of_assesment " \
            "from (SELECT tbl_courselist.id,tbl_courselist.courseName,tbl_courselbases.lbaseID " \
            " from tbl_courselist inner join tbl_courselbases on tbl_courselist.id=tbl_courselbases.courseID) " \
            "as tbl inner join tbl_lbases on tbl.lbaseID=tbl_lbases.lbaseid where tbl.id in (%s) " % ids
    # qtconn.execute(query,(ids, ))
    qtconn.execute(query)
    results = list(qtconn.fetchall())
    output_results = list()
    for result in results:
        footer = ""
        if result[2] is not None and result[2] > 0:
            footer = footer + "Videos: " + str(result[1])
        if result[3] is not None and result[3] > 0:
            if footer:
                footer = footer + " | "
            footer = footer + "Tests: " + str(result[1])
        output_results.append({
            "icon": "http://daas.peoplestrong.com/static/images/ps-capabiliti-learning-logo_v6.jpg",
            "heading_1": result[1],
            "heading_2": "Level: All Levels",
            "heading_3": "Rating: 5",
            "footer": footer,
            "user_click_actions": [
                {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                 "url": "https://mylearning.peoplestrong.com"}]
        })
    return output_results


def find_course_date(enterpriseid, qtconn, todaydate, prevdate, max_courses):  # qtrain
    """
    Finds trending courses filter by date
    :param enterpriseid: Enterpriseid
    :param qtconn: Connection to Qtrain Database
    :param todaydate: Todays Date
    :param prevdate: Prev Date
    :param max_courses: Max Courses
    :return: Courseid and Count
    """
    query = "select courseid, count(courseid) as course_count from tbl_usersubscription \
            inner join tbl_courselist on tbl_usersubscription.courseid = tbl_courselist.id \
            where tbl_usersubscription.enterpriseid=%s and tbl_usersubscription.milestone in (1, 2, 3, 4, 5) \
            and tbl_courselist.courselabel='1' and tbl_courselist.status=1 \
            and tbl_usersubscription.createdon>=%s and tbl_usersubscription.createdon<=%s \
            group by tbl_usersubscription.courseid order by course_count desc limit %s"
    qtconn.execute(query, (str(enterpriseid), prevdate, todaydate, max_courses))
    coursedate = list(qtconn.fetchall())
    return coursedate


def find_designation(empcode, tenantid, tpconn):  # TalentPact
    """
    To find employee designation
    :param empcode: Employee Code
    :param tenantid: Tenant Id
    :param tpconn: Connectio to TalentPact Database
    :return: Designationid
    """
    query = "SELECT DesignationID from HrEmployee where EmployeeCode=%s and TenantID=%s"
    tpconn.execute(query, (str(empcode), tenantid))
    designationid = list(tpconn.fetchall())
    return designationid


def get_empcode_with_same_designation(designationid, tpconn):  # Talentpact
    """
    To find peers with same designation
    :param designationid: Designation id
    :param tpconn: Connection to Talentpact Database
    :return: Employeecode
    """
    query = "SELECT EmployeeCode from HrEmployee where DesignationID=%s and EmployeeCode is not NULL"
    tpconn.execute(query, designationid)
    employeecode = list(tpconn.fetchall())
    return employeecode


def list_peer_empcodes(empcode, tenantid, tpconn):  # TalentPact
    """
    To find employee code of peers
    :param empcode: EmployeeCode
    :param tenantid: Tenantid
    :param tpconn: Connection to Talentpact Database
    :return: list of Employeecode
    """
    emplist = list()
    des = find_designation(int(empcode), tenantid, tpconn)
    empcodes = get_empcode_with_same_designation(des[0][0], tpconn)
    for i in empcodes:
        emplist.append(int(i[0]))
    return emplist
