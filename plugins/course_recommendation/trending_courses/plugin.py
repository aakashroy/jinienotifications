"""Code to find Trending Courses"""
import datetime
import traceback
from datetime import timedelta

from plugins.common import sql_helpers as c
from plugins.common.sql_helpers import connect_to_mysql_server, connect_to_sql_server
from plugins.course_recommendation.course_db_helpers import fetch_empcode_tentantid, fetch_userid_enterpriseid, \
    find_course_date, get_course_names


def fetch_basic_details(altid, mysql_conn, mssql_conn):
    """
    Fetch basic details of an user
    :param altid: Altid
    :param mysql_conn: Connection to MySql Database
    :param mssql_conn: Connection to MsSql Database
    :return: Employeecode,userid,enterpriseid,tenantid
    """
    empcode_tent_id = fetch_empcode_tentantid(altid, mssql_conn)
    employee_code, user_id, enterprise_id, tenant_id = -1, -1, -1, -1
    if empcode_tent_id['success']:
        employee_code = empcode_tent_id['emp_code']
        tenant_id = empcode_tent_id['tenant_id']
        # print employee_code,tenant_id

        userid_enterp_id = fetch_userid_enterpriseid(tenant_id, employee_code, mysql_conn)
        if userid_enterp_id['success'] is True:
            user_id = userid_enterp_id['user_id']
            enterprise_id = userid_enterp_id['enterprise_id']

    return employee_code, user_id, enterprise_id, tenant_id


def create_jinie_response(course_list):
    """
    Create response for jinie
    :param course_list: List of Courses
    :return: Response message
    """
    tmp_list = list()
    response = {"success": False}
    if course_list:
        for i in course_list:
            tmp_list.append(unicode(i['course_id']) + " : " + unicode(i['course_name']))
        courses = '<br/>'.join(tmp_list)
        final_message = "Courses are:<br/>" + courses
        response["message"] = final_message + "<br/>"
        response["success"] = True

    return response


# emp_id has been mentioned to just adhere to the function signature of run method
def run(mysql_conn=None, mssql_conn=None, emp_id=None, alt_id=None, date_time=None, max_courses=3):
    """
    Run function to run code on particular date
    :param mysql_conn: Connection to Mysql Database
    :param mssql_conn: Connection to Mssql Database
    :param alt_id: AltId
    :param date_time: DateTime
    :param max_courses: Maximum Courses
    :return: Courses on Success
    """
    is_local_conn = False
    result = {'success': False}
    try:
        if mysql_conn is None or mssql_conn is None:
            print "Creating New Connection"
            mssql_conn = c.connect_to_sql_server()
            mysql_conn = c.connect_to_mysql_server()
            is_local_conn = True

        if date_time is not None:
            employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
            if employee_code > 0 and user_id > 0 and enterprise_id > 0 and tenant_id > 0:
                date = date_time.date()
                today = date.today()
                todaydatetime = str(date) + ' ' + "00:00:00"
                since_weeks = 1
                trending_courses = list()
                while since_weeks <= 2:
                    date = date - timedelta(days=since_weeks)
                    prevdate = str(
                        today - datetime.timedelta(days=today.weekday(), weeks=since_weeks)) + ' ' + "00:00:00"
                    courses = find_course_date(enterprise_id, mysql_conn, todaydatetime, prevdate, max_courses)
                    trending_active_courses = [int(i[0]) for i in courses]

                    if len(trending_active_courses) >= 3 or since_weeks == 2:
                        if trending_active_courses:
                            string_top_3_courseid = ','.join([unicode(i) for i in trending_active_courses])
                            trending_courses = get_course_names(string_top_3_courseid, mysql_conn)
                            break

                    since_weeks = since_weeks + 1

                if trending_courses:
                    result["success"] = True
                    result["message"] = {
                        "payload": trending_courses,
                        "layout": "card",
                        "greeting": __greeting()
                    }
            else:
                result['error_message'] = "fetch_basic_details returned -1 for one or more Ids"
        else:
            result['error_message'] = "date_time argument is None"
    except Exception:
        result['error_message'] = "Exception while constructing trending course notification"
        result['error_traceback'] = traceback.format_exc()

    if is_local_conn:
        mysql_conn.close()
        mssql_conn.close()
    return result


def service_name():
    """
    Name of Services
    :return: service name
    """
    return "trending_courses"


def __greeting():
    """
    Greeting Meeting
    :return: Greeting with user name
    """
    return "Hi ##NAME##, here are the courses which have been trending!"


if __name__ == "__main__":
    print run(connect_to_mysql_server(), connect_to_sql_server(), 2205395, date_time=datetime.datetime.now(),
              max_courses=3)
