# Rules
# For correct value, a whole number will be returned
# For any incorrect condition choose a negative number and return that
import datetime
import traceback
from datetime import timedelta

from plugins.common import sql_helpers as c


def query_excuter(crsr, emp_id, date_time):
    """
    Execute query to find pending leaves approvals
    :param crsr: Connection to Talentpact Database
    :param emp_id: Employeeid
    :param date_time: datetime
    :return: Number of pending leave Approvals
    """
    try:
        tomorrow = date_time + timedelta(days=1)
        start_dt = datetime.datetime.combine(tomorrow.date(),
                                             datetime.datetime.strptime('00:00:00', '%H:%M:%S').time())
        end_dt = datetime.datetime.combine(tomorrow.date(),
                                           datetime.datetime.strptime('23:59:59', '%H:%M:%S').time())
        managerid = emp_id
        manager_finder = "select COUNT(lvs.Employeeid) as Employees,Sum(lvs.dates) as Leaves from " \
                         "(Select l.Employeeid,COUNT(l.TheDate) as dates from " \
                         "(SELECT TheDate,dt.Employeeid,dt.statusmessage,FromDateid from TimeDimension as td inner join " \
                         "(select Fromdateid,Employeeid,statusmessage from TmEmployeeLeave where EmployeeID in " \
                         "(select employeeid from hremployee with (nolock) where l1managerid=%s) " \
                         "and statusmessage like '%pending%' and IsActive=1) as dt " \
                         "on dt.Fromdateid=td.TimeDimensionID and TheDate>=%s and TheDate<=%s) as l " \
                         "GROUP by l.Employeeid) as lvs"
        crsr.execute(manager_finder, (managerid, start_dt, end_dt))
        manager = crsr.fetchall()
        data = {'success': False}
        if manager[0][0] != 0:
            # print 3/0
            data['message'] = "Hi ##NAME## you have <b>%d</b> Leave Approval Pending from <b>%d</b> Employees" % (
                manager[0][1], manager[0][0])
            data['success'] = True
        return data

    except Exception:
        data = dict()
        data['success'] = False
        data['error_message'] = "Exception while trying to get leaves data"
        data['error_traceback'] = traceback.format_exc()
        return data  # return -3 if there is exception


def run(date_time, crsr=None, email_id=None, emp_id=None, alt_id=None):
    """
    To run query and check connection to Database
    :param date_time: Datatime
    :param crsr: Connection to TalentPact Database
    :param email_id: Emailid
    :param emp_id: Employeeid
    :param dummy_alt_id: Altid
    :return: Number of pending leaves for approval
    """
    is_local_conn = False
    result = {'success': False}
    try:
        if crsr is None:
            print "Creating New Connection"
            crsr = c.connect_to_sql_server()
            is_local_conn = True

        if date_time is not None:
            if emp_id is not None:
                result = query_excuter(crsr, emp_id, date_time)
            elif email_id is not None:
                employee_id = "select  top 1 Employeeid  from dbo.hremployee with (nolock) where OfficialEmailAddress=%s"
                crsr.execute(employee_id, email_id)
                emp_id = crsr.fetchall()
                if emp_id:
                    result = query_excuter(crsr, emp_id[0][0], date_time)
    except Exception:
        result['error_message'] = "Exception while constructing leaves notification"
        result['error_traceback'] = traceback.format_exc()

    finally:
        if is_local_conn:
            crsr.close()
    return result


def service_name():
    """
    Name of service to be run
    :return: Service name
    """
    return "leaves"


if __name__ == "__main__":
    print run(date_time=datetime.datetime.now(), emp_id=950952)
