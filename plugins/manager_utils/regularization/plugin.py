"""This is plugin for number of regularizations to approved by Manager"""

# Rules
# For correct value, a whole number will be returned
# For any incorrect condition choose a negative number and return that

import datetime
import traceback
from datetime import timedelta

from plugins.common import sql_helpers as c


def query_excuter(crsr, emp_id, date_time):
    """
    Execute query to find pending regularizations approvals
    :param crsr: Connection to Talentpact Datebase
    :param emp_id: Employeeid
    :param date_time: Datatime
    :return: Number of pending Regularizations
    """
    try:
        data = {'success': False}
        now = date_time
        prev = now - timedelta(days=90)

        now = datetime.datetime.combine(now.date(), datetime.datetime.strptime('23:59:59', '%H:%M:%S').time())
        prev = datetime.datetime.combine(prev.date(), datetime.datetime.strptime('00:00:00', '%H:%M:%S').time())

        managerid = emp_id
        manager_finder = "SELECT count(employeeid) as employees,sum(reg) as pending_reg from" \
                         "(select employeeid,COUNT(1) as reg from TmEmployeeAttendanceReg with (nolock) where " \
                         "employeeid in (select employeeid from hremployee with (nolock) where " \
                         "l1managerid=%s and ApplicationDate>=%s and ApplicationDate<=%s " \
                         ") and Status like '%pending%' group by employeeid) as tmp"
        crsr.execute(manager_finder, (managerid, prev, now))
        manager = crsr.fetchall()
        if manager[0][0] != 0:
            # print 3/0
            data['message'] = "Hi ##NAME## you have <b>%d</b> Regularization Pending from <b>%d</b> Employees" % (
                manager[0][1], manager[0][0])
            data['success'] = True

        return data  # no pending regularization

    except Exception:
        data = dict()
        data['success'] = False
        data['error_message'] = "Exception while trying to get regularization data"
        data['error_traceback'] = traceback.format_exc()
        return data  # return -3 if there is exception


def run(date_time, crsr=None, email_id=None, emp_id=None, alt_id=None):
    """
    To run query and check connection to Database
    :param date_time: Datetime
    :param crsr: Connection to Talentpact Database
    :param email_id: Emailid
    :param emp_id: Employeeid
    :param alt_id: Altid
    :return: Pending Regularization
    """
    is_local_conn = False
    result = {'success': False}
    try:
        if crsr is None:
            print "Creating New Connection"
            crsr = c.connect_to_sql_server()
            is_local_conn = True

        if date_time is not None:
            if emp_id is not None:
                result = query_excuter(crsr, emp_id, date_time)
            elif email_id is not None:
                employee_id = "select  top 1 Employeeid  from dbo.hremployee with (nolock) where OfficialEmailAddress=%s"
                crsr.execute(employee_id, email_id)
                emp_id = crsr.fetchall()
                if emp_id:
                    result = query_excuter(crsr, emp_id[0][0], date_time)
    except Exception:
        result['error_message'] = "Exception while constructing regularization notification"
        result['error_traceback'] = traceback.format_exc()

    finally:
        if is_local_conn:
            crsr.close()
    return result


def service_name():
    '''name of service to be run'''
    return "regularization"


if __name__ == "__main__":
    print run(date_time=datetime.datetime.now(), emp_id=28267)
