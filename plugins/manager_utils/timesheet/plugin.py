"""This is plugin for number of timesheets to approved by Manager"""

import datetime
import traceback
from datetime import timedelta

from plugins.common import sql_helpers as c


def query_excuter(crsr, emp_id, date_time):
    """
    Execute query to find pending timesheets approvals
    :param crsr: Coonction to Talentpact Database
    :param emp_id: Employeeid
    :param date_time: Datetime
    :return: Number of pending timesheet approvals
    """
    try:
        data = {'success': False}
        now = date_time
        prev = now - timedelta(days=90)

        now = datetime.datetime.combine(now.date(),
                                        datetime.datetime.strptime('23:59:59', '%H:%M:%S').time())
        prev = datetime.datetime.combine(prev.date(),
                                         datetime.datetime.strptime('00:00:00', '%H:%M:%S').time())

        managerid = emp_id
        manager_finder = "select COUNT(t.EmployeeId) as Employees,sum(t.TmSum) as Pending_TS from (select EmployeeId, " \
                         "count(1) as TmSum from TmEmployeeTimesheet with (nolock) where (WorkflowStatus='Approval Pending' " \
                         "or WorkflowStatus='Pending for Approval' " \
                         "or WorkflowStatus='pending') and ApplicationDate>=%s and ApplicationDate<=%s " \
                         "and employeeid in (select employeeid from hremployee with (nolock) where l1managerid=%s) and isActive=1" \
                         "group by EmployeeId) as t"
        crsr.execute(manager_finder, (prev, now, managerid))
        manager = crsr.fetchall()
        if manager[0][0] != 0:
            # print 3/0
            data['message'] = "Hi ##NAME## you have <b>%d</b> Timesheets Pending from <b>%d</b> Employees" % (
                manager[0][1], manager[0][0])
            data['success'] = True
        return data

    except Exception:
        data = dict()
        data['success'] = False
        data['error_message'] = "Exception while trying to get timesheet data"
        data['error_traceback'] = traceback.format_exc()
        return data  # return -3 if there is exception


def run(date_time, crsr=None, email_id=None, emp_id=None, alt_id=None):
    """
    To run query and check connection to Database
    :param date_time: Datetime
    :param crsr: Connection to Talentpact Database
    :param email_id: Emailid
    :param emp_id: Employeeid
    :param alt_id: Altid
    :return: Pending Timesheets
    """
    is_local_conn = False
    result = {'success': False}
    try:
        if crsr is None:
            print "Creating New Connection"
            crsr = c.connect_to_sql_server()
            is_local_conn = True

        if date_time is not None:

            if emp_id is not None:
                result = query_excuter(crsr, emp_id, date_time)
            elif email_id is not None:
                employee_id = "select  top 1 Employeeid  from dbo.hremployee with (nolock) where OfficialEmailAddress=%s"
                crsr.execute(employee_id, email_id)
                emp_id = crsr.fetchall()
                if emp_id:
                    result = query_excuter(crsr, emp_id[0][0], date_time)
        else:
            return {'success': False}  # Date is none
    except Exception:
        result['error_message'] = "Exception while constructing timesheet notification"
        result['error_traceback'] = traceback.format_exc()

    finally:
        if is_local_conn:
            crsr.close()
    return result


def service_name():
    '''name of service to be run'''
    return "timesheet"


if __name__ == "__main__":
    print run(date_time=datetime.datetime.now(), emp_id=28267)
