import Queue
import json
import sys
import traceback
from time import sleep

import pysolr
import requests

from simplex_communications.event_consumers import send_notfn_to_jinie

DEBUG_FLAG = 0


def get_team_member_details_from_solr(email, org_id):
    solrdb = pysolr.Solr('http://10.226.0.242:8983/solr/alt-search')
    result = solrdb.search(q="official_email:\"{email}\" AND org_id:{org_id}".
                           format(email=email, org_id=org_id), fl="first_name,id",
                           rows=1)
    result = [x for x in result]
    return result[0] if result else []


def get_team_string(level):
    if level == 1:
        return "L1"
    elif level == 2:
        return "L1 and L2"
    else:
        return "entire"


def deque_qilo_reminder(message_dict):
    qilo_header = message_dict['qilo_header']
    user_id = int(message_dict['params']['request_user_id'])
    request_email = str(message_dict['params']['request_email'])
    level = int(message_dict['params']['level'])
    days = int(message_dict['params']['days'])
    qilo_org_id = int(message_dict['params']['qilo_org_id'])
    org_id = int(message_dict['params']['org_id'])
    requesting_user_name = None

    team_queue = Queue.Queue()
    users_set = dict()

    team_queue.put({'email': request_email})
    users_set[request_email] = {'email': request_email}

    total_team_members = 0

    while not team_queue.empty():
        team_member = team_queue.get()
        more_team_member_details = get_team_member_details_from_solr(team_member["email"], int(org_id))
        if more_team_member_details:
            print("Processing: {}".format(more_team_member_details['first_name']))
            if not requesting_user_name:
                requesting_user_name = more_team_member_details['first_name']

            team_member.update(more_team_member_details)
            subteam_members = get_qilo_team_okr(qilo_org_id, team_member["id"], team_member["email"], qilo_header)
            total_team_members = total_team_members + len(subteam_members)
            print("Total team members: {}".format(total_team_members))
            sleep(0.1)
            if subteam_members:
                subteam_members[-1]["is_last_member"] = True
                for item in subteam_members:
                    if item['email'] not in users_set:
                        users_set[item['email']] = item
                        team_queue.put(item)
            if level == 1:
                break
            elif level == 2 and team_member.get("is_last_member", False):
                break

    while not team_queue.empty():
        team_member = team_queue.get()
        more_team_member_details = get_team_member_details_from_solr(team_member["email"], int(org_id))
        if more_team_member_details:
            team_member.update(more_team_member_details)
            users_set[team_member['email']] = team_member

    users_set.pop(request_email)

    reminder_user_count = 0
    for _, user in users_set.items():
        print(user)
        if "id" in user and int(user['last_checkin']) > int(days):
            receiver_id = int(user['id'])
            receiver_name = str(user['first_name'])
            print("sending notification to " + str(receiver_name))
            msg = "Hi " + receiver_name + ", please check-in your OKR.<br/>" \
                                          "<br/>Thanks!<br/>Jinie on behalf of " + str(
                requesting_user_name)
            if DEBUG_FLAG == 0:
                sleep(0.1)
                send_notfn_to_jinie.send(receiver_id, msg)
            print(receiver_id, msg)
            print(reminder_user_count)
            reminder_user_count = reminder_user_count + 1
    if reminder_user_count == 0:
        msg = "Hi " + str(requesting_user_name) + ", your " + get_team_string(
            level) + " team did a great job! I found that all of your team members " \
                     "have checked-in within in the last " + str(days) + " days"
        if DEBUG_FLAG == 0:
            send_notfn_to_jinie.send(user_id, msg)
        print(user_id, msg)
        return reminder_user_count
    else:
        total_members = len(users_set)
        msg = "Hi " + str(requesting_user_name) + ", I have reminded " + str(
            reminder_user_count) + " people from your " + get_team_string(level) + " team (out of " \
              + str(total_members) + ") to check-in."
        if DEBUG_FLAG == 0:
            send_notfn_to_jinie.send(user_id, msg)
        print(user_id, msg)
        return reminder_user_count


def get_qilo_team_okr(qilo_org_id, user_id, email, header):
    result = []
    try:
        api_url = "https://performanceapi.peoplestrong.com/api/v1/rest/reporteeList"
        headers = header
        data = {"emp_code": str(user_id), "org_id": qilo_org_id, "emp_email": email}
        print data
        res = requests.post(api_url, json=data, headers=headers)
        print res.text
        json_data = json.loads(res.text)
        if isinstance(json_data, list):
            for user in json_data:
                try:
                    user_checkin_days = int(user["checkin_days_ago"])
                except Exception:
                    user_checkin_days = sys.maxint
                user_data = {"last_checkin": user_checkin_days,
                             "email": user["email"]}
                result.append(user_data)
        return result
    except Exception:
        exception_string = traceback.format_exc()
        print str(exception_string)
        result = []
    return result


if __name__ == '__main__':
    message_dict = dict()
    message_dict['qilo_header'] = {
        "api-access-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI4MSIsImlzcyI6Imh0dHBzOi8vd3d3LnFpbG90ZWNoLmNvbSJ9.lf__uPT3itUQ-7ohBrM-fOi5Xpi5l8GX1JGIZYc-kqo",
        "api-key": "81HI28BB4D86PPK3QK3H2",
        "api-org": "81",
        "Content-Type": "application/json"
    }
    message_dict['params'] = dict()
    message_dict['params']['request_user_id'] = 2060312
    message_dict['params']['request_email'] = "harsimran.walia@zippi.co"
    message_dict['params']['level'] = 3
    message_dict['params']['days'] = 15
    message_dict['params']['qilo_org_id'] = 81
    message_dict['params']['org_id'] = 19
    deque_qilo_reminder(message_dict)

    exit(1)

    # get_qilo_team_okr(19,11)
    headers = {
    }
    postdata = {"org_id": "19",
                "qilo_org_id": "81",
                "user_id": "2060312",
                "email_id": "harsimran.walia@zippi.co",
                "level": "L2",
                "days": "10",
                "domain": "hrms.peoplestrong.com"
                }
    print(type(postdata))
    # qilo_team_reminders(postdata, headers)
    # qilo_team_remainders(19, 81, 1749823, "mukesh.antil@peoplestrong.com", 2, 10)
