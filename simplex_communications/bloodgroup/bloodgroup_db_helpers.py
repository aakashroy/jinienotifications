"""It contians all queries require to fetch blood donation related details"""
import traceback

from plugins.common.sql_helpers import connect_to_sql_server
from blood_group_normalization import blood_group_normalization


def recipient_details(talentpact_conn, alt_id):
    """
    Fetches Recipient Details
    :param talentpact_conn: Connection to TalentPact DataBase
    :param alt_id: EmployeeId
    :return: Fullname, Worksiteid, Worksite name, Tenant id, Contact no
    """
    result = None
    query = "Select per.GivenName,per.MiddleName,per.FamilyName,hre.EmployeeID,ct1.Type,hrw.WorkSiteName," \
            "hre.WorkSiteID,hre.TenantID,hrp.MobileNumber from dbo.HrEmployee as hre inner join dbo.HrContentType" \
            " as ct1 on hre.EmploymentStatusID=ct1.TypeID inner join HrProfile as hrp on hrp.ProfileId=hre.ProfileID " \
            "inner join dbo.HrPerson per on per.PersonID=hrp.PersonID inner join HrWorkSite as hrw on hrw.WorkSiteID=" \
            "hre.WorkSiteID where  hre.UserID=%s"
    talentpact_conn.execute(query, (alt_id))
    details = talentpact_conn.fetchone()
    if details:
        name = [details[0], details[1], details[2]]
        fullname = ' '.join(x for x in name if x)
        worksite_name = details[5]
        worksite_id = details[6]
        tenant_id = details[7]
        contact_no = details[8]

        result = fullname, worksite_id, worksite_name, tenant_id, contact_no
    return result


def donor_altids(talentpact_conn, bloodgroup, tenant_id, worksite_id):
    """
    List of All Employees with required blood group
    :param talentpact_conn: Connection to TalentPact DataBase
    :param bloodgroup: BloogGroup
    :param tenant_id: Tenant ID
    :param worksite_id: WorksiteId
    :return: List of Altids
    """
    try:
        query = "Select hre.UserID from dbo.HrEmployee as hre inner join dbo.HrContentType as ct " \
                "on hre.BloodGroupTypeID=ct.TypeID inner join dbo.HrContentType as ct1 on hre.EmploymentStatusID" \
                "=ct1.TypeID inner join HrWorkSite as hrw on hrw.WorkSiteID=hre.WorkSiteID where ct.Type in ({}) and " \
                "hre.TenantID=%s and hre.WorkSiteID=%s and ct1.Type like 'active'".format(bloodgroup)
        talentpact_conn.execute(query, (tenant_id, worksite_id))
        result = talentpact_conn.fetchall()
        alt_ids = None
        if result:
            alt_ids = [x[0] for x in result]
        return alt_ids
    except Exception:
        traceback.print_exc()


if __name__ == "__main__":
    CONN = connect_to_sql_server()
    RECIPIENT_DETAILS = recipient_details(CONN, 2205395)

    BLOOD_GROUPS = blood_group_normalization('O-ve')
    if BLOOD_GROUPS:
        LIST_OF_BLOOD_GROUPS = ','.join('\'' + b + '\'' for b in BLOOD_GROUPS)
        print LIST_OF_BLOOD_GROUPS
        LIST_OF_DONORS_ALTID = donor_altids(CONN, LIST_OF_BLOOD_GROUPS, 10, 17557)
        if LIST_OF_DONORS_ALTID:
            print LIST_OF_DONORS_ALTID
        else:
            print "Not Found"
    else:
        print "Not Found"
    CONN.close()
