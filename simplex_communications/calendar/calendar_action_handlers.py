"""Calander action handlers"""
from common import alt_jinie_actions, o365_helpers, mongo_helpers, jinie_notification_templates
from common.generic_functions import get_first_name
from simplex_communications import config


def process_action_on_meeting(alt_id, meeting_id, action):
    """
    This function looks at the action to be performed and constructs
    corresponding message list which will be sent by the caller
    as Jinie notifications
    :param alt_id:
    :param meeting_id:
    :param action:
    :return:
    """
    user_details = o365_helpers.get_user_data(alt_id, config)
    if user_details is None:
        return []
    meeting = o365_helpers.get_event(meeting_id, user_details, config)
    if meeting is None:
        return []
    if not o365_helpers.is_event_yet_to_start(meeting, False)["will_start"]:
        return []
    # meeting = o365_helpers.get_event(meeting_id, user_details, config, select="all")

    organiser_email = meeting['organizer']['emailAddress']['address']
    organiser = mongo_helpers.get_user_details_from_email(organiser_email, config)

    messages = []
    if organiser is not None:
        message_str = ""
        if action == alt_jinie_actions.ACTION_CALENDAR_NOT_ATTENDING:
            message_str = jinie_notification_templates.CALENDAR_ACTION_NOTATTENDING.format(
                get_first_name(user_details["first_name"], user_details["middle_name"], user_details["last_name"]),
                meeting["subject"]
            )
        elif action == alt_jinie_actions.ACTION_CALENDAR_LATE:
            message_str = jinie_notification_templates.CALENDAR_ACTION_LATE.format(
                get_first_name(user_details["first_name"], user_details["middle_name"], user_details["last_name"]),
                meeting["subject"]
            )

        if message_str:
            messages.append((organiser["alt_id"], message_str))

    return messages


def process_action_on_org_late_meeting(alt_id, meeting_id):
    """
    This functions is to process action on organizer late metting
    :param alt_id:
    :param meeting_id:
    :return:
    """
    user_details = o365_helpers.get_user_data(alt_id, config)
    if user_details is None:
        return []
    meeting = o365_helpers.get_event(meeting_id, user_details, config)
    if meeting is None:
        return []
    if not o365_helpers.is_event_yet_to_start(meeting, False)["will_start"]:
        return []

    message_str = jinie_notification_templates.CALENDAR_ACTION_ORGANIZER_LATE.format(
        meeting["subject"]
    )

    messages = []

    for attendee in meeting['attendees']:
        attendee_email = attendee['emailAddress']['address']
        attendee_mongo = mongo_helpers.get_user_details_from_email(attendee_email, config)
        if attendee_mongo is not None:
            messages.append((attendee_mongo["alt_id"], message_str))

    return messages


if __name__ == "__main__":
    process_action_on_meeting(2205395,
                              u'AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgFRAAgI1k2x8vIAAEYAAAAAeXmwD5rWmE2Vkb3DiKew1wcA-cQ7HC8S5kCZqpSnvmM-RwAAAAABDQAA-cQ7HC8S5kCZqpSnvmM-RwAAZyVagAAAEA==',
                              1)
