import re
import string
import traceback
from datetime import datetime, timedelta

import pymongo
import pytz
import requests
from tzlocal import get_localzone

from common import web_helpers
from common.o365_helpers import get_user_data


def get_user_details_from_name(name, conf):
    """
    Get user details from name
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({
            "$or": [
                {'search_f_name': name},
                {'search_fm_name': name},
                {'search_fl_name': name},
                {'search_fml_name': name}
            ]
        }).limit(2)
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result


def find_meeting_times_o365(sender_data, config, attendees, timeconstraint, max_candidates=2):
    fmt_url = config.MS_GRAPH_ENDPOINT.format('/me/findMeetingTimes')
    query_parameters = {
        "maxCandidates": max_candidates,
        "minimumAttendeePercentage": 100
    }
    query_parameters.update(attendees)
    query_parameters.update(timeconstraint)
    req = web_helpers.make_api_call('POST', fmt_url, sender_data['access_token'],
                                    payload=query_parameters)
    if req.status_code == requests.codes.ok:
        return req.json()
    return None


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def clean_string(inputstr):
    outputstr = inputstr.lower()
    if isinstance(outputstr, unicode):
        outputstr = translate_punctuation_to_empty(outputstr)
    else:
        outputstr = outputstr.translate(None, string.punctuation)
    outputstr = re.sub(r'\s+', '', outputstr)
    return outputstr.strip()


def convert_to_utc(datetimeobj):
    utc = datetimeobj.astimezone(pytz.timezone("UTC"))
    return utc


def convert_from_anytz_to_local(datetimestr, timezonestr):
    timezone = pytz.timezone(timezonestr)
    dtm_musecs = datetimestr.split(".")
    tz_aware_dt = timezone.localize(datetime.strptime(dtm_musecs[0], "%Y-%m-%dT%H:%M:%S"))
    return tz_aware_dt.astimezone(get_localzone())


def get_time_constraint(timeslots):
    timeslots_ms_format = list()
    for timeslot in timeslots:
        start_t = convert_to_utc(timeslot[0])
        start = start_t.strftime('%Y-%m-%dT%H:%M:%S')
        end_t = convert_to_utc(timeslot[1])
        end = end_t.strftime('%Y-%m-%dT%H:%M:%S')
        timeslots_ms_format.append({
            "start": {
                "dateTime": start,
                "timeZone": str(start_t.tzinfo)
            },
            "end": {
                "dateTime": end,
                "timeZone": str(end_t.tzinfo)
            }
        })
    return {
        "timeConstraint": {
            "activityDomain": "work",
            "timeslots": timeslots_ms_format
        }
    }


def generate_timeslots():
    timeslots = list()
    local_tz = get_localzone()

    today = local_tz.localize(datetime.now())

    tomorrow = today + timedelta(days=1)

    day_after_tomorrow = today + timedelta(days=2)

    later_this_week = today + timedelta(days=4 - today.weekday())

    later_next_week_start = today + timedelta(days=0 - today.weekday() + 7)

    later_next_week_end = later_next_week_start + timedelta(days=4)

    # oob_today = local_tz.localize(datetime.strptime(
    #     "{0}-{1}-{2} 00:00:00".format(today.year, today.month, today.date),
    #     "%Y-%m-%d %H:%M:%S"
    # ))

    cob_today = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(today.year, today.month, today.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_tomorrow = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(tomorrow.year, tomorrow.month, tomorrow.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_tomorrow = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(tomorrow.year, tomorrow.month, tomorrow.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_later_this_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(day_after_tomorrow.year, day_after_tomorrow.month, day_after_tomorrow.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_later_this_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(later_this_week.year, later_this_week.month, later_this_week.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_later_next_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(later_next_week_start.year, later_next_week_start.month,
                                      later_next_week_start.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_later_next_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(later_next_week_end.year, later_next_week_end.month, later_next_week_end.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    if today.weekday() < 5:
        timeslots.append({
            "type": "Today",
            "start": today,
            "end": cob_today,
            "avlabl": True
        })
    else:
        timeslots.append({
            "type": "Today",
            "avlabl": False
        })

    if tomorrow.weekday() < 5:
        timeslots.append({
            "type": "Tomorrow",
            "start": oob_tomorrow,
            "end": cob_tomorrow,
            "avlabl": True
        })
    else:
        timeslots.append({
            "type": "Tomorrow",
            "avlabl": False
        })

    if later_this_week.weekday() < 3:
        timeslots.append({
            "type": "Later this week",
            "start": oob_later_this_week,
            "end": cob_later_this_week,
            "avlabl": True
        })
    else:
        timeslots.append({
            "type": "Later this week",
            "avlabl": False
        })

    timeslots.append({
        "type": "Next week",
        "start": oob_later_next_week,
        "end": cob_later_next_week,
        "avlabl": True
    })

    return timeslots


def get_ms_formatted_attendee_list(m_attendees):
    attendees = {'attendees': list()}
    for m_attendee in m_attendees:
        attendees['attendees'].append({
            "type": "required",
            "emailAddress": {
                "address": m_attendee["email"],
            }
        })
    return attendees


def get_message_from_ecode(ecode):
    messages = [
        "",
        "I failed to lookup one or more of the users you mentioned. Please contact support.",
        "Looks like there is either a typo in a name or the user does not exist. Please fix and ask me again.",
        "Looks like one of the names you specified is ambiguous. Please add more characters to the name and try again.",
        "I failed to lookup one or more of the users you mentioned. Please contact support.",
        "I failed to lookup one or more of the users you mentioned. Please contact support."
    ]
    return messages[ecode]


def process_find_meeting_times(names, alt_id, config):
    results = list()
    ecode = 0
    cleaned_names = list()
    for name in names:
        name = name.lower()
        names_in_name = name.split("and")
        for new_name in names_in_name:
            cleaned_names.append(clean_string(new_name))

    users_in_meeting = list()
    for name in cleaned_names:
        try:
            m_results = get_user_details_from_name(name, config)
            if not m_results:
                ecode = 2
                break
            elif len(m_results) != 1:
                ecode = 3
                break
            users_in_meeting.extend(m_results)
        except Exception:
            ecode = 1
            traceback.print_exc()
            break

    if ecode == 0:
        my_details = get_user_data(alt_id, config)
        if my_details:
            users_in_meeting.append(my_details)
        else:
            ecode = 4

    if ecode == 0:
        try:
            timeslots = generate_timeslots()
            attendees = get_ms_formatted_attendee_list(users_in_meeting)
            for timeslot in timeslots:
                result = dict()
                result["dow"] = timeslot["type"]
                result["slots"] = list()
                if timeslot["avlabl"]:
                    o365_res = find_meeting_times_o365(
                        my_details,
                        config,
                        attendees,
                        get_time_constraint([(timeslot["start"], timeslot["end"])])
                    )
                    print json.dumps(o365_res, indent=4)
                    if o365_res:
                        for mtgtms in o365_res["meetingTimeSuggestions"]:
                            localstart = convert_from_anytz_to_local(mtgtms["meetingTimeSlot"]["start"]["dateTime"],
                                                                     mtgtms["meetingTimeSlot"]["start"]["timeZone"])
                            localend = convert_from_anytz_to_local(mtgtms["meetingTimeSlot"]["end"]["dateTime"],
                                                                   mtgtms["meetingTimeSlot"]["end"]["timeZone"])
                            result["slots"].append((localstart, localend))
                results.append(result)
        except:
            traceback.print_exc()
            ecode = 5

    if ecode == 0:
        return {'success': True, 'resuls': results}
    else:
        message = get_message_from_ecode(ecode)
        return {'success': False, 'results': {'ecode': ecode, 'message': message}}


if __name__ == "__main__":
    from simplex_communications import config
    import json

    print json.dumps(process_find_meeting_times(["Aakash roY and Pankaj Bansal", "Amber Nigam"], 2205395, config),
                     indent=4, default=unicode)
