"""Project Configuration"""
MONGO_CONNECTION = "mongodb://appuser:password@mongo1.peoplestrong.com:27017"

MONGO_DB_ALT_JINIE = "altjinie"
MONGO_COLL_ORG_MAPPING = "org_config"
MONGO_COLL_USERS = "users"

SQL_CREDENTIALS = {"IP": "10.226.0.187", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867",
                   "DB": "TalentPact"}  ## Prod

MYSQL_CREDENTIALS = {"IP": "13.71.27.119", "PORT": "3306", "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355",
                     "DB": "qtrain"}

SUBDOMAIN = "https://daas.peoplestrong.com"

ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE = "/queue/user_registration_inlimbo_queue"
ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE = "/queue/notification_sending_queue"
ACTIVE_MQ_USER_SCHEDULING_QUEUE = "/queue/user_scheduling_queue"
ACTIVE_MQ_INCOMING_EMAIL_QUEUE = "/queue/incoming_email_queue"

ACCESS_TOKEN_TIMEOUT_THRESHOLD = 10 * 60  # In seconds

CLIENT_ID = '2861a4b4-6853-4318-ac15-2a623d10a66b'
CLIENT_SECRET = 'lbWEOHJR12=mkctxX102^(+'
# REDIRECT_URI = 'https://3810c094.ngrok.io/login/authorized'
REDIRECT_URI = SUBDOMAIN + '/login/authorized'
o365_EMAIL_WEBHOOK_CREATED_ENDPOINT = SUBDOMAIN + "/listeners/emails/o365/created"
o365_EMAIL_APPROVE_ACTION_ENDPOINT = SUBDOMAIN + "/actions/o365/emails/approve?id={}&alt_id={}&frm={}&subj={}"

AUTHORITY_URL = 'https://login.microsoftonline.com'
AUTH_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/authorize?{0}')
TOKEN_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/token')
MS_GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0{0}'

SCOPES = [
    'openid',
    'offline_access',  # This is the most imp, for getting refresh token!
    'Calendars.Read',
    'Calendars.Read.Shared',
    'Calendars.ReadWrite.Shared',
    'Calendars.ReadWrite',
    'Contacts.Read',
    'Contacts.Read.Shared',
    'Mail.Read',
    'Mail.Read.Shared',
    'People.Read',
    'User.Read',
    'Mail.Send'
]  # Add other scopes/permissions as needed.

MYSQL_CONNECTION_EJABBERD = {
    "host": "192.168.2.114",
    "user": "admin",
    "passwd": "admin@123",
    "database": "ejabberd"}

# ACTIVEMQ_CONNECTION = [('10.226.0.76', 61613)]
# ACTIVEMQ_CONNECTION = [('127.0.0.1', 61613)]
ACTIVEMQ_CONNECTION = [('10.226.0.45', 61613)]

DEFAULT_CALENDAR_NOTIFICATION_MINUTES_AGO = 15

MEETING_ACTIONS_URI_PREFIX = SUBDOMAIN + "/actions/calendar/"

PLUGIN_DIR = "/home/aakash.roy@psnet.com/projx/jinienotifications/plugins"
# PLUGIN_DIR = "/home/aakash/work/projx/jinienotifications/plugins"
PLUGIN_PKG_NAME = "plugins"
PLUGIN_RUN_METHOD_NAME = "run"
PLUGIN_SVC_NAME_METHOD_NAME = "service_name"

CALENDAR_EVENT_TOO_EARLY_THRESHOLD_SEC = 300

FAKE_JINIE_API_FOR_INTERNAL_TESTING = False

TIME_ZONE_DISPLAYMAP = {
    "Asia/Calcutta": "IST",
    "Asia/Kolkata": "IST",
    "Asia/Singapore": "SGT"
}
