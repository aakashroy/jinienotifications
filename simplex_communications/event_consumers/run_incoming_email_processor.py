"""Process Notification Queue"""
import base64
import json
import re
import time
import traceback

from spacy.lang.en import English

from common import activemq_helpers, logging
from common.mongo_helpers import get_user_details_from_o365_email_created_subscription_creator_id
from common.o365_helpers import get_email, get_user_data
from simplex_communications import config
from simplex_communications.event_consumers import send_notfn_to_jinie

re_HTML = re.compile(r'<.*?>')
re_HTML_DotAll = re.compile(r'<.*?>', flags=re.S)
APPROVAL_PHRASES = [
    "please approve",
    "kindly approve",
    "need your approval",
    "require your approval",
    "approve kindly",
    "give your approval",
    "your approval is needed",
    "require approval",
    "request approval",
    "provide your approval"
]


def incoming_email_processor():
    """
    Proc Notification Queue Worker
    :return:
    """

    def work(headers, message, message_id, subscription, activemq_handle):
        """
        Work
        :param headers:
        :param message:
        :param message_id:
        :param subscription:
        :param activemq_handle:
        :return:
        """
        variable_stack = dict()
        variable_stack['headers'] = headers
        variable_stack['message_id'] = message_id
        variable_stack['subscription'] = subscription
        variable_stack['message'] = message
        logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER", "Picked up user's request",
                    variable_stack)
        start = time.time()
        try:
            handled = False
            message_dict = json.loads(message)
            if "action" in message_dict and message_dict["action"] == "email/o365/created":
                logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER",
                            "Processing Email to check if there is Approval")
                handled = True
                for value in message_dict["postdata"]["value"]:
                    print("DS Correct")
                    o365_user_id = value["resourceData"]["@odata.id"].split("/")[1]
                    user = get_user_details_from_o365_email_created_subscription_creator_id(o365_user_id, config)
                    if user:
                        print("User Correct")
                        user_data = get_user_data(user["alt_id"], config)
                        email = get_email(value["resourceData"]["id"], user_data, config, "all")
                        print("Email details", email.get("subject", "NoSubject"), email.get("from", "NoFrom"),
                              email.get("toRecipients", "NoRecipients"))
                        if not email["isRead"]:
                            _email_body = re.sub(re_HTML, '', email["body"]["content"])
                            _email_body = re.sub(re_HTML_DotAll, '', _email_body)
                            approval_sents = []
                            first_sentence = ""
                            if "P.S: Approval Powered by Peoplestrong Jinie" not in _email_body \
                                    and "approved" not in _email_body.lower():
                                for email_body in re.split(r"[\r\n]", _email_body):
                                    email_body = email_body.strip()
                                    if len(email_body) == 0:
                                        continue
                                    nlp = English()
                                    sentencizer = nlp.create_pipe("sentencizer")
                                    nlp.add_pipe(sentencizer)
                                    doc = nlp(email_body)
                                    for sent in doc.sents:
                                        sent_ = str(sent)
                                        lower_fs = sent_.lower()
                                        if not first_sentence and len(sent_.split()) > 1 \
                                                and not re.findall("^(?:from:|sent:|to:|subject:|hi|hello|dear)",
                                                                   lower_fs):
                                            first_sentence = sent_
                                        sent_ = sent_.lower()
                                        sent_ = re.sub(r"\s+", " ", sent_)
                                        for approval_phrase in APPROVAL_PHRASES:
                                            if approval_phrase in sent_:
                                                approval_sents.append(sent_)

                                if approval_sents:
                                    # from_zone = tz.gettz("UTC")
                                    # to_zone = tz.tzlocal()
                                    # sent_date_time = datetime.strptime(email["sentDateTime"], "%Y-%m-%dT%H:%M:%SZ")
                                    # sent_date_time = sent_date_time.replace(tzinfo=from_zone)
                                    # sent_date_time = sent_date_time.astimezone(to_zone)

                                    email_subject = re.sub("\s+", " ", email["subject"])
                                    email_subject = re.sub("\r", '', email_subject)
                                    email_subject = re.sub("\n", '', email_subject)
                                    email_subject = re.sub(re_HTML, '', email_subject)
                                    email_subject = re.sub("\s+", " ", email_subject)
                                    email_subject = email_subject.strip()

                                    message = dict()
                                    message["layout"] = "meeting_reminder_card"
                                    message["payload"] = dict()
                                    # message["payload"]["greeting"] = "Hi {}, you have a request for approval!".format(
                                    #     user_data["first_name"])
                                    message["payload"]["metg_title"] = email_subject
                                    message["payload"][
                                        "metg_loc"] = "Attached: Expense Report"
                                    users_to_send_notfn = []

                                    # to_receipients = []
                                    # for receipient in email["toRecipients"]:
                                    #     # to_receipients.append({
                                    #     #     "name": receipient["emailAddress"]["name"],
                                    #     #     "id": receipient["emailAddress"]["address"]
                                    #     # })
                                    #     to_user = get_user_details_from_email_v2(receipient["emailAddress"]["address"], config)[
                                    #         "alt_id"]
                                    #     users_to_send_notfn.append(to_user)

                                    message["payload"]["metg_members"] = [
                                        {
                                            "name": email["from"]["emailAddress"]["name"],
                                            "id": email["from"]["emailAddress"]["address"]
                                        }
                                    ]
                                    message["payload"]["metg_time"] = "Urgency: Normal"
                                    message["payload"]["metg_actions"] = [
                                        # {
                                        #     "val": email["webLink"],
                                        #     "label": "Open in outlook"
                                        # },
                                        {
                                            "val": config.o365_EMAIL_APPROVE_ACTION_ENDPOINT.format(
                                                value["resourceData"]["id"],
                                                user["alt_id"],
                                                base64.urlsafe_b64encode(email["from"]["emailAddress"]["address"]),
                                                base64.urlsafe_b64encode(email_subject)
                                            ),
                                            "label": "Approve"
                                        }
                                    ]

                                    message["payload"]["greeting"] = "You have a request for approval! <br/> " \
                                                                     "<i>\"{}\"</i>".format(
                                        first_sentence)
                                    if send_notfn_to_jinie.send(user["alt_id"], message):
                                        logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER",
                                                    "Approval Notification SENT",
                                                    {"approver": user["alt_id"], "payload": message})
                                    else:
                                        logging.log(logging.SEVERITY_CRITICAL, "INCOMING-MAIL-PROCESSOR-WORKER",
                                                    "Approval Notification NOT SENT",
                                                    {"approver": user["alt_id"], "payload": message})

            if handled == False:
                logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER", "Unknown action type received",
                            message_dict)
        except Exception:
            logging.log(logging.SEVERITY_CRITICAL, "INCOMING-MAIL-PROCESSOR-WORKER", traceback.format_exc(),
                        variable_stack)

        end = time.time()

        logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER", "Took: " + str(end - start) + " seconds")

    return work


def main():
    """
    Main Function
    :return:
    """
    name = "Incoming Email Processor"
    activemq_helpers.run_worker(name, incoming_email_processor(), config,
                                config.ACTIVE_MQ_INCOMING_EMAIL_QUEUE)


if __name__ == "__main__":
    main()
    # pre_process_calendar_notification('AAMkAGFhMDg1MTA0LWU1ZGEtNGZkMi1iNWRiLTdiNmY0YWMxOGZlYgBGAAAAAABHdNRe3OkPTo7jb7PAkfR0BwCaN0tg34aIT6VuliQZXwBDAAAAAAENAACaN0tg34aIT6VuliQZXwBDAAEaP9MtAAA=', 2060312)
