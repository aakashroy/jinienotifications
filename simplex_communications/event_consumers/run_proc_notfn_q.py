"""Process Notification Queue"""
import json
import time
import traceback

import stomp

from common import alt_jinie_topics, o365_helpers, alt_jinie_actions, activemq_helpers, logging, optimizations
from helpers.db_helpers import connect_to_sql_server
from simplex_communications import config
from simplex_communications.bloodgroup.blood_group_normalization import blood_group_normalization
from simplex_communications.bloodgroup.bloodgroup_db_helpers import recipient_details, donor_altids
from simplex_communications.calendar import calendar_action_handlers
from simplex_communications.event_consumers import send_notfn_to_jinie


def proc_notfn_q_worker():
    """
    Proc Notification Queue Worker
    :return:
    """

    def work(headers, message, message_id, subscription, activemq_handle):
        """
        Work
        :param headers:
        :param message:
        :param message_id:
        :param subscription:
        :param activemq_handle:
        :return:
        """
        variable_stack = dict()
        variable_stack['headers'] = headers
        variable_stack['message_id'] = message_id
        variable_stack['subscription'] = subscription
        variable_stack['message'] = message
        logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER", "Picked up user's request",
                    variable_stack)
        start = time.time()
        try:
            message_dict = json.loads(message)
            alt_id = message_dict['destination_alt_user_id']

            messages = []

            if 'message' in message_dict:
                messages = [message_dict['message']] if not isinstance(message_dict['message'], list) else message_dict[
                    'message']
                messages = [(alt_id, message) for message in messages]

            should_send = True

            if message_dict['message_type'] == alt_jinie_topics.CALENDAR:
                logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER", "CALENDAR(EVENT) Message Type")
                if 'notfn_id' in message_dict:
                    pre_proc_status = o365_helpers.pre_process_calendar_notification(message_dict['notfn_id'], alt_id,
                                                                                     config)
                    if pre_proc_status == 1:
                        should_send = True
                    else:
                        should_send = False
                        if pre_proc_status > 1:
                            logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER",
                                        "Too Early (" + str(
                                            config.CALENDAR_EVENT_TOO_EARLY_THRESHOLD_SEC) +
                                        " sec) for CALENDAR(EVENT) Message (Maybe rescheduled?), "
                                        "Rescheduling")
                            headers = dict({
                                'persistent': 'true'
                            })
                            local_queue_conn = activemq_handle is None or not activemq_handle.is_connected()

                            if local_queue_conn:
                                activemq_handle = stomp.Connection(config.ACTIVEMQ_CONNECTION)
                                activemq_handle.start()
                                activemq_handle.connect()
                            headers['AMQ_SCHEDULED_DELAY'] = int(pre_proc_status * 1000)  # In milliseconds
                            dummy_response = activemq_handle.send(
                                destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                body=optimizations.json_dumps_minify(message_dict),
                                headers=headers)
                            if local_queue_conn:
                                activemq_handle.disconnect()
                            logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER",
                                        "Calendar Event Rescheduled with delay of " + str(pre_proc_status) + " seconds",
                                        message_dict)
                else:
                    should_send = False
            elif message_dict['message_type'] == alt_jinie_topics.ACTION:
                alt_id = message_dict['destination_alt_user_id']
                meeting_id = message_dict['action'].get('meeting_id', "")
                action = message_dict['action']['action']
                if action == alt_jinie_actions.NOTIFY_BLOOD_DONORS:
                    messages = []
                    blood_group = message_dict['action']['blood_group']
                    conn = connect_to_sql_server(config)
                    recipient_info = recipient_details(conn, alt_id)
                    if recipient_details:
                        tenant_id = recipient_info[3]
                        work_group_id = recipient_info[1]
                        contact_no = recipient_info[4]
                        name = recipient_info[0]
                        blood_groups = blood_group_normalization(blood_group)

                        if blood_groups:
                            message = "<b>" + name + "</b> requires " \
                                                     "<b>" + blood_groups[
                                          0] + "</b> blood. Please reach out to them at " \
                                               "below number if you are interested " \
                                               "to donate.<br/><br/><a href='tel:" + contact_no + "'>" + contact_no + "<br/>"
                            list_of_blood_groups = ','.join('\'' + b + '\'' for b in blood_groups)
                            list_of_donors_altid = donor_altids(conn, list_of_blood_groups, tenant_id, work_group_id)
                            if list_of_donors_altid:
                                for donor in list_of_donors_altid:
                                    messages.append((str(donor), message))
                elif action == alt_jinie_actions.ACTION_CALENDAR_NOT_ATTENDING \
                        or action == alt_jinie_actions.ACTION_CALENDAR_LATE:
                    messages = calendar_action_handlers.process_action_on_meeting(alt_id, meeting_id, action)
                elif action == alt_jinie_actions.ACTION_CALENDAR_ORGANIZER_LATE:
                    messages = calendar_action_handlers.process_action_on_org_late_meeting(alt_id, meeting_id)
            elif message_dict['message_type'] == alt_jinie_topics.PLUGIN \
                    or message_dict['message_type'] == alt_jinie_topics.MOTD:
                pass
            else:
                should_send = False

            if should_send:
                logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER",
                            "Should send is True and there are " + str(len(messages)) + " to send")
                for (alt_id, mesg) in messages:
                    logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER",
                                "Sending Jinie Notification to " + str(alt_id), {'message': mesg})
                    if send_notfn_to_jinie.send(alt_id, mesg):
                        logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER",
                                    "Notification SENT")
                    else:
                        logging.log(logging.SEVERITY_CRITICAL, "NOTFN-Q-WORKER",
                                    "Notification NOT SENT")
            else:
                logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER", "Should send is false")

        except Exception:
            logging.log(logging.SEVERITY_CRITICAL, "NOTFN-Q-WORKER", traceback.format_exc(),
                        variable_stack)

        end = time.time()

        logging.log(logging.SEVERITY_DEBUG, "NOTFN-Q-WORKER", "Took: " + str(end - start) + " seconds")

    return work


def main():
    """
    Main Function
    :return:
    """
    name = "Notification Sending Queue Processor"
    activemq_helpers.run_worker(name, proc_notfn_q_worker(), config,
                                config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE)


if __name__ == "__main__":
    main()
    # proc_notfn_q_worker()({},
    #                       '{"action": {"action": 4, "blood_group": "A+"}, "message_type": 2, "destination_alt_user_id": "2205395"}',
    #                       "1",
    #                       "",
    #                       None)
