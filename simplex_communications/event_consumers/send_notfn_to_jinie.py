"""Sends Notification to Jinie"""
import json
import time
import traceback

import requests
from jiniepush import jinie_notifications

from common import logging
from simplex_communications import config


def get_generic_design_layout_deprecated(alt_id, text_components):
    final_layout = {
        "input": {
            "messageType": "NOTIFICATION",
            "usersList": [
                {
                    "messageType": "NOTIFICATION",
                    "userId": alt_id,
                    "text": text_components
                }
            ]
        }
    }
    return final_layout


def safe_print_deprecated(to_print):
    """
    Function to safely print statements without crashing the app
    :param to_print: The string to print
    :return: None
    """
    if isinstance(to_print, unicode):
        to_print = to_print.encode("utf8")
    print to_print


def deprecated__log(log_text, show):
    """
    This methods prints log_text to the console

    :param log_text: The text to print

    :param show: Whether to display the log_text

    :return: None
    """
    if show:
        safe_print_deprecated(log_text)


def send_generic_design_deprecated(alt_id, payload, verbose=False):
    """

    :param alt_id: The Alt ID of the user.
    This is the Id present in the ejabberd table.
    Get it from the mobile team! messenger.mobile@peoplestrong.com
    :param payload: The payload to render. This is the new stylish UI!
    :param verbose: Set this to True to see debug messages
    :return: True if sending was successful, False otherwise.
    Raises exceptions for erroneous conditions
    """

    deprecated__log("Function send_generic_design called with arguments:", verbose)
    deprecated__log("alt_id: " + unicode(alt_id), verbose)
    deprecated__log("payload: " + unicode(payload), verbose)

    if not isinstance(alt_id, basestring):
        raise RuntimeError("Input argument alt_id should be of type str")

    if not isinstance(payload, list):
        raise RuntimeError("Input argument card_layout should be of type list")

    deprecated__log("Input arguments are okay", verbose)

    notification_body = get_generic_design_layout_deprecated(alt_id, payload)

    deprecated__log("About to send notification:\n" + json.dumps(notification_body, indent=4, default=str), verbose)

    response = requests.post(url="https://messenger.peoplestrong.com/custom/jinieNotificationRequest",
                             headers={"Content-Type": "application/json",
                                      "Authorization": "JinieNotificationITGGN575"},
                             data=json.dumps(notification_body, separators=(',', ':')),
                             timeout=30)

    deprecated__log("Received response: " + json.dumps(response, indent=4, default=str), verbose)

    if response.status_code == 200:
        return True

    return False


def send(alt_id, message):
    """
    Function to send notification
    :param alt_id: Altid
    :param message: Message
    :return:
    """
    start = time.time()
    result = False
    try:
        if config.FAKE_JINIE_API_FOR_INTERNAL_TESTING:  # or (isinstance(message, basestring) and "blood" in message):
            print " = = = Sent Jinie Notification as follows = = = "
            print " ALT ID (RECEIVER): " + str(alt_id)
            print " MESSAGE: " + str(message)
            print " = = = = = = = = = = = = = = = = = = = = = = = "
            result = True
        else:
            if isinstance(message, dict) and "layout" in message:
                if message["layout"] == "card":
                    result = jinie_notifications.send_cards(str(alt_id), message["greeting"], message["payload"],
                                                            verbose=True)
                elif message["layout"] == "meeting_reminder_card":
                    result = True
                    result = jinie_notifications.send_meeting_reminder_card(str(alt_id), message["payload"]["greeting"],
                                                                            message["payload"]["metg_title"],
                                                                            message["payload"]["metg_time"],
                                                                            message["payload"]["metg_loc"],
                                                                            message["payload"]["metg_members"],
                                                                            message["payload"]["metg_actions"],
                                                                            verbose=True)

                elif message["layout"] == "generic-design":
                    # result_dep = True
                    # if message.get("depricated_payload", None):
                    #     result_dep = send_generic_design_deprecated(str(alt_id), message["depricated_payload"],
                    #                                                 verbose=True)
                    #     logging.log(logging.SEVERITY_DEBUG, "JINIE-NOTFN-API-SEND",
                    #                 "Deprecated API Result: " + str(result_dep))
                    #     # time.sleep(0.5)
                    result = send_generic_design_deprecated(str(alt_id), message["payload"],
                                                            verbose=True)
                    logging.log(logging.SEVERITY_DEBUG, "JINIE-NOTFN-API-SEND", "New API Result: " + str(result))
                    # result = result & result_dep
                else:
                    logging.log(logging.SEVERITY_CRITICAL, "JINIE-NOTFN-API-SEND",
                                "Unkown message layout!", {'message': message})
            else:
                result = jinie_notifications.send_simple(alt_id=str(alt_id), message=message, format_message=False,
                                                         verbose=True)
    except:
        logging.log(logging.SEVERITY_CRITICAL, "JINIE-NOTFN-API-SEND",
                    "Failed to deliver notification to Jinie Endpoint: \n" + traceback.format_exc(),
                    {'message': message})

    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "JINIE-NOTFN-API-SEND", "Took: " + str(end - start) + " seconds")
    return result


if __name__ == "__main__":
    ##jinie_notifications.send_simple("2205395", "Check 1 2 3 Testing Jinie Notification Delivery", verbose=True)
    messege = {
        "layout": "meeting_reminder_card",
        "payload": {
            "greeting": "Edukondalu, you have an upcoming meeting!",
            "metg_loc": "  ",
            "metg_members": [
                {
                    "name": "Devendra Tripathi",
                    "id": "devendra.tripathi@peoplestrong.com"
                }
            ],
            "metg_actions": [
                {
                    "val": "https://outlook.office365.com/owa/?itemid=AAMkADI0YmM4M2IwLWY4MzctNDkyZi1hZWZmLWM4ODAxODdiODJhMQBGAAAAAAC14syDGyyuRqO0SCrg54HCBwBCiVha6EubQbxGc5KNUsIqAAAAAAENAABCiVha6EubQbxGc5KNUsIqAACq0G%2B6AAA%3D&exvsurl=1&path=/calendar/item",
                    "label": "Open in outlook"
                }
            ],
            "metg_title": "test meeting",
            "metg_time": "6:30PM - 7:00PM"
        }
    }
    send(2721489, messege)
