"""Create Calendar Notification"""
import copy
import time
import urllib
from datetime import timedelta, datetime
from tzlocal import get_localzone
from dateutil import tz

import stomp

from common import generic_functions, jinie_notification_templates, jinie_notification_messaging_templates, \
    alt_jinie_topics, o365_helpers, alt_jinie_actions, logging, optimizations
from common.generic_functions import get_first_name
from simplex_communications import config


def create_calendar_notfns_and_push(alt_id, activemq_conn=None, events=None, user_data=None, send_now=False):
    """
    Create calendar notification and push it.
    :param alt_id:
    :param activemq_conn:
    :param events:
    :param user_data:
    :return:
    """
    start = time.time()
    now = datetime.now()
    if user_data is None:
        user_data = o365_helpers.get_user_data(alt_id, config)
    logging.log(logging.SEVERITY_DEBUG, "EVENTS", "Creating", user_data)
    if user_data is not None:
        user_timezone = user_data.get("timezone", str(get_localzone()))
        if user_timezone == '': user_timezone = str(get_localzone())
        timezone_displaystr = config.TIME_ZONE_DISPLAYMAP.get(user_timezone, user_timezone)
        if events is None:
            # Convert to user's local
            local_datetime = o365_helpers.convert_datetime_timezone(datetime.utcnow(),"UTC", user_timezone)
            today = local_datetime.date()
            start_time = local_datetime.time()
            end_time = datetime.strptime('23:59:59.999999', '%H:%M:%S.%f').time()
            # tomorrow = today + timedelta(days=1)
            # Covert back to UTC

            events = o365_helpers.get_events(user_data, today, today, start_time, end_time, config, user_timezone)
            events = o365_helpers.filter_events(events, False, today, today, start_time, end_time, user_timezone)

        if events is not None:
            for event in events['value']:
                messages_to_enq = {}
                messages_to_enq["layout"] = "meeting_reminder_card"
                messages_to_enq["payload"] = {}
                messages_to_enq["payload"]["metg_actions"] = []

                start_datetimestr = datetime.strptime(event['start']['dateTime'], '%Y-%m-%dT%H:%M:%S.%f0')
                end_datetimestr = datetime.strptime(event['end']['dateTime'], '%Y-%m-%dT%H:%M:%S.%f0')
                orig_notification_date_time = o365_helpers.convert_datetime_timezone(
                        start_datetimestr,
                        event['start']['timeZone'], user_timezone)
                reminder_mins_before_start = event['reminderMinutesBeforeStart'] \
                    if event['reminderMinutesBeforeStart'] > 0 \
                    else config.DEFAULT_CALENDAR_NOTIFICATION_MINUTES_AGO

                notification_date_time = orig_notification_date_time - timedelta(
                    minutes=reminder_mins_before_start)

                #now = datetime.now()
                now = o365_helpers.convert_datetime_timezone(datetime.utcnow(), "UTC", user_timezone)
                scheduled_delay = int((notification_date_time - now).total_seconds() * 1000)
                orig_delay = int((orig_notification_date_time - now).total_seconds() * 1000)

                if orig_delay >= 0:

                    if scheduled_delay <= 0 or orig_delay == 0 or send_now:
                        scheduled_delay = None
                    metg_actions = []
                    messages_to_enq["payload"]["metg_members"] = []
                    greeting_string = jinie_notification_templates.CALENDAR_GREETING.format(
                        get_first_name(user_data["first_name"], user_data["middle_name"], user_data["last_name"]))
                    messages_to_enq["payload"]["greeting"] = greeting_string
                    messages_to_enq["payload"]["metg_title"] = event['subject']
                    if not event['isAllDay']:
                        start_time = o365_helpers.convert_datetime_timezone(start_datetimestr, event['start']['timeZone'],
                                                               user_timezone)

                        end_time = o365_helpers.convert_datetime_timezone(end_datetimestr, event['end']['timeZone'],
                                                               user_timezone)
                        meeting_time_string = jinie_notification_templates.CALENDAR_MEETING_TIME.format(
                            start_time.strftime("%-I:%M%p"),end_time.strftime("%-I:%M%p"))
                        messages_to_enq["payload"]["metg_time"] = meeting_time_string+" ("+timezone_displaystr+")"

                    else:
                        messages_to_enq["payload"]["metg_time"] = jinie_notification_templates.CALENDAR_MEETING_ALL_DAY

                    if 'location' in event and 'displayName' in event['location'] and event['location']['displayName']:
                        messages_to_enq["payload"]["metg_loc"] = jinie_notification_templates. \
                            CALENDAR_REMINDER_MEETING_LOCATION.format(event['location']['displayName'])
                    else:
                        messages_to_enq["payload"]["metg_loc"] = "Not Declared"

                    if event['attendees']:
                        limbo_names = [{"name": x['emailAddress']['name'], "id": x['emailAddress']['address']} for x in
                                       event['attendees'] if x['type'] != "resource"]
                        messages_to_enq["payload"]["metg_members"] = limbo_names
                    else:
                        messages_to_enq["payload"]["metg_members"] = [{"name": "No Attendees", "id": ""}]
                    meeting_late_uri = config.MEETING_ACTIONS_URI_PREFIX \
                                       + event['id'] + "/" \
                                       + str(alt_id) + "/" + alt_jinie_actions.ACTION_CALENDAR_LATE_STR

                    meeting_org_late_uri = config.MEETING_ACTIONS_URI_PREFIX \
                                           + event['id'] + "/" \
                                           + str(alt_id) + "/" + alt_jinie_actions.ACTION_CALENDAR_ORGANIZER_LATE_STR

                    meeting_not_going_uri = config.MEETING_ACTIONS_URI_PREFIX \
                                            + event['id'] + "/" \
                                            + str(alt_id) + "/" + alt_jinie_actions.ACTION_CALENDAR_NOT_ATTENDING_STR

                    quoted_id = urllib.quote_plus(event['id'])
                    weblink = "https://outlook.office.com/calendar/item/{}".format(quoted_id)
                    metg_actions.append({"label": jinie_notification_templates.CALENDAR_LINK, "val": weblink})
                    if not event['isOrganizer']:
                        metg_actions.append(
                            {"label": jinie_notification_templates.CALENDAR_LATE_LINK, "val": meeting_late_uri})

                        metg_actions.append({"label": jinie_notification_templates.CALENDAR_NOT_GOING_LINK,
                                             "val": meeting_not_going_uri})
                    else:
                        metg_actions.append(
                            {"label": jinie_notification_templates.CALENDAR_ORG_LATE_LINK, "val": meeting_org_late_uri})

                    messages_to_enq["payload"]["metg_actions"] = metg_actions

                    should_send = True
                    if should_send:
                        logging.log(logging.SEVERITY_DEBUG, "EVENTS", "Enqueuing", {"events": messages_to_enq})
                        local_queue_conn = activemq_conn is None or not activemq_conn.is_connected()
                        if local_queue_conn:
                            activemq_conn = stomp.Connection(config.ACTIVEMQ_CONNECTION)
                            activemq_conn.start()
                            activemq_conn.connect()

                        message = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_RESPONSE)
                        message['notfn_id'] = event['id']
                        message['message_type'] = alt_jinie_topics.CALENDAR
                        message['destination_alt_user_id'] = alt_id
                        message['message'] = messages_to_enq
                        headers = {
                            'persistent': 'true'
                        }

                        if scheduled_delay is not None:
                            headers['AMQ_SCHEDULED_DELAY'] = int(scheduled_delay)  # In milliseconds
                            logging.log(logging.SEVERITY_DEBUG, "EVENTS",
                                        "Message scheduled with delay: " + str(headers['AMQ_SCHEDULED_DELAY']) + " ms.")
                        else:
                            logging.log(logging.SEVERITY_DEBUG, "EVENTS",
                                        "Message is scheduled for immediate delivery")

                        dummy_response = activemq_conn.send(destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                                            body=optimizations.json_dumps_minify(message),
                                                            headers=headers)

                        print "Pushed message at: " + str(datetime.now())

                        if local_queue_conn:
                            activemq_conn.disconnect()

                        logging.log(logging.SEVERITY_DEBUG, "EVENTS", "Enqueued", message)
                    else:
                        logging.log(logging.SEVERITY_DEBUG, "EVENTS", "Not enqueued")

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "EVENTS", "Took: " + str(end - start) + " seconds")

    return user_data, events


if __name__ == "__main__":
    # print timeit.timeit('push_motd(2205395)', 'from __main__ import push_motd', number=10)
    # for i in range(0, 20):
    # create_calendar_notfns_and_push(2205395)
    create_calendar_notfns_and_push(3218689, send_now=True)
    # create_calendar_notfns_and_push(2060312)
    # create_motd_and_push(2060312, scheduled_delay=int(15000), onboarding=True)
    # push_motd(2060312)
