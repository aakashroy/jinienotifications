"""Create message of the day(MOTD)"""
import copy
import time
from datetime import timedelta, datetime

import stomp

from common import generic_functions, logging, optimizations
from common import jinie_notification_messaging_templates
from common import jinie_notification_templates
from common import o365_helpers, alt_jinie_topics
from common.generic_functions import get_first_name
from common.mongo_helpers import update_o365_email_created_webhook_id
from common.o365_helpers import register_email_created_webhook
from simplex_communications import config
from tzlocal import get_localzone


def create_motd_and_push(alt_id, scheduled_delay=None, activemq_conn=None, onboarding=False, qotd=None):
    """
    Create message of the day and push it.
    :param alt_id:
    :param scheduled_delay:
    :param activemq_conn:
    :param onboarding:
    :param qotd: Quote of the day, if available
    :return:
    """
    start = time.time()
    events = None
    deprecated_motd_string = ""  # All usages of this string will be @Deprecated
    motd_message = dict()
    user_data = o365_helpers.get_user_data(alt_id, config)
    logging.log(logging.SEVERITY_DEBUG, "MOTD", "Creating", user_data)
    if user_data is not None:
        user_timezone = user_data.get("timezone", str(get_localzone()))
        if user_timezone=='' : user_timezone = str(get_localzone())
        timezone_displaystr = config.TIME_ZONE_DISPLAYMAP.get(user_timezone, user_timezone)
        utc_now = datetime.utcnow()
        user_datetime = o365_helpers.convert_datetime_timezone(datetime.utcnow(), "UTC", user_timezone)
        #now = datetime.now()
        now =user_datetime
        #today = utc_now.date()
        today = utc_now.date()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)
        response = register_email_created_webhook(user_data, config)
        if response and "id" in response:
            expdt = datetime.strptime(response["expirationDateTime"], "%Y-%m-%dT%H:%M:%SZ")
            update_o365_email_created_webhook_id(alt_id, response["id"], response["creatorId"], expdt, config)

        unread_emails = o365_helpers.get_unread_emails(user_data, yesterday, tomorrow, config)
        unread_emails_focused = [x for x in unread_emails['value'] if x['inferenceClassification'] == "focused"]
        unread_emails['values'] = unread_emails_focused

        start_time = now.time() if onboarding else datetime.strptime('00:00:00.000000', '%H:%M:%S.%f').time()
        end_time = datetime.strptime('23:59:59.999999', '%H:%M:%S.%f').time()
        user_today_date = user_datetime.date()
        # r = o365_helpers.get_event_list(user_data, config)
        events = o365_helpers.get_events(user_data, user_today_date, user_today_date, start_time, end_time, config, user_timezone)
        filter_start_time = user_datetime.time()
        events = o365_helpers.filter_events(events, onboarding, user_today_date, user_today_date, filter_start_time, end_time, user_timezone)
        if unread_emails is not None and events is not None:
            unread_email_count = len(unread_emails['value'])
            important_email_count = len([x for x in unread_emails['value'] if x['importance'] == 'high'])
            event_count = len(events['value'])
            is_more_email = unread_emails.get('max_calls_reached', False)
            deprecated_motd_string = jinie_notification_templates.MOTD_GREETING.format(
                generic_functions.get_greeting(now),
                get_first_name(user_data["first_name"],
                               user_data["middle_name"],
                               user_data["last_name"]))
            if unread_email_count > 0:
                deprecated_motd_string = deprecated_motd_string + "<br/><br/>"
                deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_UNREAD_MAILS. \
                    format(str(unread_email_count) + (" or more " if is_more_email else ""))
                if important_email_count > 0:
                    deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_IMP_MAILS.format(
                        str(important_email_count) + (" or more " if is_more_email else ""),
                        "is" if important_email_count == 1 else "are"
                    )
            deprecated_motd_string = deprecated_motd_string + "<br/><br/>"
            deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_MEETINGS. \
                format("no" if event_count == 0 else event_count,
                       "" if event_count == 1 else "s")

            motd_message["greeting"] = deprecated_motd_string

            if event_count > 0:
                deprecated_motd_string = deprecated_motd_string + "<br/>"

            motd_message["list_of_meetings"] = list()
            for i in range(0, event_count):
                """
                mtg_start_time = generic_functions.convert_datetime_with_zone_to_local(
                    events['value'][i]['start']['dateTime'],
                    events['value'][i]['start']['timeZone'])
                mtg_end_time = generic_functions.convert_datetime_with_zone_to_local(
                    events['value'][i]['end']['dateTime'],
                    events['value'][i]['end']['timeZone'])
                """
                start_datetimestr = datetime.strptime(events['value'][i]['start']['dateTime'], '%Y-%m-%dT%H:%M:%S.%f0')
                end_datetimestr = datetime.strptime(events['value'][i]['end']['dateTime'], '%Y-%m-%dT%H:%M:%S.%f0')
                start_time = o365_helpers.convert_datetime_timezone(start_datetimestr, events['value'][i]['start']['timeZone'],
                                                                    user_timezone)

                end_time = o365_helpers.convert_datetime_timezone(end_datetimestr, events['value'][i]['start']['timeZone'],
                                                                  user_timezone)

                mtg_start_time = start_time.strftime("%-I:%M%p")
                mtg_end_time = end_time.strftime("%-I:%M%p")

                ## Deprecated Starts
                deprecated_motd_string = deprecated_motd_string + "<br/>"
                deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_EACH_MEETING. \
                    format(mtg_start_time, mtg_end_time, events['value'][i]['subject'])
                ## Deprecated Ends
                motd_message["list_of_meetings"].append(
                    {
                        "title": events['value'][i]['subject'],
                        "description": mtg_start_time + " - " + mtg_end_time +" ("+timezone_displaystr+")"
                    }
                )

            if not qotd:
                qotd = (
                    "It does not matter how slowly you go as long as you do not stop.",
                    "Confucius")

            if qotd:
                deprecated_motd_string = deprecated_motd_string + "<br/><br/>"
                deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_QOTD.format(qotd[0],
                                                                                                                qotd[1])
                motd_message["quote"] = qotd[0]
                motd_message["author"] = qotd[1]
                motd_message["ending"] = jinie_notification_templates.MOTD_ENDING

            deprecated_motd_string = deprecated_motd_string + "<br/><br/>"
            deprecated_motd_string = deprecated_motd_string + jinie_notification_templates.MOTD_ENDING + "<br/>"
    else:
        logging.log(logging.SEVERITY_ERROR, "MOTD", "User data fetch / refresh error")

    if motd_message:
        motd_message = jinie_notification_templates.get_jinie_motd_template(motd_message)
        motd_deprecated_message = jinie_notification_templates.get_jinie_design_label_template([deprecated_motd_string])
        logging.log(logging.SEVERITY_DEBUG, "MOTD", "Enqueuing", {"motd_message": motd_message,
                                                                  "deprecated_motd_message": motd_deprecated_message})
        local_queue_conn = activemq_conn is None or not activemq_conn.is_connected()

        if local_queue_conn:
            activemq_conn = stomp.Connection(config.ACTIVEMQ_CONNECTION)
            activemq_conn.start()
            activemq_conn.connect()

        message = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_RESPONSE)
        message['message_type'] = alt_jinie_topics.MOTD
        message['destination_alt_user_id'] = alt_id
        message['message'] = {"layout": "generic-design", "payload": motd_message,
                              "depricated_payload": motd_deprecated_message}
        headers = {
            'persistent': 'true'
        }
        if scheduled_delay is not None:
            headers['AMQ_SCHEDULED_DELAY'] = int(scheduled_delay)  # In milliseconds
            logging.log(logging.SEVERITY_DEBUG, "MOTD",
                        "Message scheduled with delay: " + str(headers['AMQ_SCHEDULED_DELAY']) + " ms.")
        else:
            logging.log(logging.SEVERITY_DEBUG, "MOTD",
                        "Message is scheduled for immediate delivery")

        dummy_response = activemq_conn.send(destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                            body=optimizations.json_dumps_minify(message),
                                            headers=headers)

        if local_queue_conn:
            activemq_conn.disconnect()

        logging.log(logging.SEVERITY_DEBUG, "MOTD", "Enqueued", message)
    else:
        logging.log(logging.SEVERITY_ERROR, "MOTD", "No MOTD String to send")

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "MOTD", "Took: " + str(end - start) + " seconds")

    return (user_data, events)


if __name__ == "__main__":
    # print timeit.timeit('push_motd(2205395)', 'from __main__ import push_motd', number=10)
    create_motd_and_push(2205395)
    # create_motd_and_push(2060312, onboarding=True)
    # create_motd_and_push(2060312, scheduled_delay=int(15000), onboarding=True)
    # push_motd(2060312)
