"""Create plugin Notifications'"""
import copy
import time
import traceback

import stomp

from common import o365_helpers, jinie_notification_messaging_templates, alt_jinie_topics, logging, optimizations
from common.generic_functions import get_first_name
from simplex_communications import config


def get_plugin_notfn_message(alt_id, run_fn, user_data, scheduled_on):
    """
    Get Plugin notification message
    :param alt_id:
    :param run_fn:
    :param user_data:
    :param scheduled_on:
    :return:
    """
    start = time.time()
    message = ""
    logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "Creating",
                {'user_data': user_data, 'scheduled_on': scheduled_on})
    try:
        if user_data is None:
            user_data = o365_helpers.get_user_data(alt_id, config)
        if user_data is not None:
            logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "Before calling fn")
            result = run_fn(emp_id=user_data['emp_id'], alt_id=user_data['alt_id'], date_time=scheduled_on)
            logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "After calling fn")
            # result = run_fn(date_time=scheduled_on)
            logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE",
                        "Received result from plugin fn",
                        {'name': str(run_fn), 'result': result})
            if result['success'] is True:
                logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "Result received", result)
                message = result['message']
                if isinstance(message, basestring):
                    message = message.replace("##NAME##",
                                              get_first_name(user_data["first_name"], user_data["middle_name"],
                                                             user_data["last_name"]))
                else:
                    if message["layout"] == "card":
                        message["greeting"] = message["greeting"].replace("##NAME##",
                                                                          get_first_name(user_data["first_name"],
                                                                                         user_data["middle_name"],
                                                                                         user_data["last_name"]))
                    else:
                        logging.log(logging.SEVERITY_CRITICAL, "PLUGIN-CREATE",
                                    "Unkown message layout!", message)
                logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "Final Message", {"message": message})
            else:
                logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "No Result received", result)
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "PLUGIN-CREATE",
                    traceback.format_exc(), user_data)
    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "PLUGIN-CREATE", "Took: " + str(end - start) + " seconds")

    return message


def send(alt_id, plugin_message, scheduled_delay, activemq_conn):
    """
    Sends Message
    :param alt_id: Altid
    :param plugin_message: plugin Message
    :param scheduled_delay: Scheduled Delay
    :param activemq_conn: Active MQ Connection
    :return:
    """
    start = time.time()
    logging.log(logging.SEVERITY_DEBUG, "PLUGIN-SEND", "Enqueing")

    if scheduled_delay <= 0:
        scheduled_delay = None

    local_queue_conn = activemq_conn is None or not activemq_conn.is_connected()

    if local_queue_conn:
        activemq_conn = stomp.Connection(config.ACTIVEMQ_CONNECTION)
        activemq_conn.start()
        activemq_conn.connect()

    message = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_RESPONSE)
    message['message_type'] = alt_jinie_topics.PLUGIN
    message['destination_alt_user_id'] = alt_id
    message['message'] = plugin_message
    headers = {
        'persistent': 'true'
    }

    if scheduled_delay is not None:
        headers['AMQ_SCHEDULED_DELAY'] = int(scheduled_delay)  # In milliseconds
        logging.log(logging.SEVERITY_DEBUG, "PLUGIN-SEND",
                    "Message scheduled with delay: " + str(headers['AMQ_SCHEDULED_DELAY']) + " ms.")
    else:
        logging.log(logging.SEVERITY_DEBUG, "PLUGIN-SEND",
                    "Message is scheduled for immediate delivery")

    dummy_response = activemq_conn.send(destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                        body=optimizations.json_dumps_minify(message),
                                        headers=headers)

    if local_queue_conn:
        activemq_conn.disconnect()

    logging.log(logging.SEVERITY_DEBUG, "PLUGIN-SEND", "Enqueued", message)

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "PLUGIN-SEND", "Took: " + str(end - start) + " seconds")
