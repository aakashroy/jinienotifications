"""Module for testing plugins"""
from datetime import datetime

from common.plugin_helpers import PluginClass
from simplex_communications import config
from simplex_communications.event_producers import create_plugin_notfns

PLUGIN_OBJ = PluginClass()
PLUGINS = PLUGIN_OBJ.get_plugins(config.PLUGIN_DIR, config.PLUGIN_PKG_NAME,
                                 config.PLUGIN_RUN_METHOD_NAME,
                                 config.PLUGIN_SVC_NAME_METHOD_NAME)
for (run_fn, svc_name_fn) in PLUGINS:
    service_name = svc_name_fn()
    scheduled_on = datetime.now()
    message, schedule = create_plugin_notfns.get_plugin_notfn_message(2653216, run_fn, None,
                                                                      scheduled_on)
    if message and schedule is not None:
        schedule = datetime.combine(scheduled_on.date(), schedule)
        create_plugin_notfns.send(2653216, message, schedule, None)
