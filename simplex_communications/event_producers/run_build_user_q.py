"""Run and Build User Queue"""
import copy
import sys
import time
import traceback
import unicodedata
from datetime import datetime
from tzlocal import get_localzone

import pymongo
import requests
import stomp

from common import jinie_notification_messaging_templates, alt_jinie_topics, logging, optimizations
from simplex_communications import config


def enqueue_user_from_mongo_to_motd_q(conf, scheduled_time):
    """
    Enqueue user from mongoDB to MOTD queue
    :param conf:
    :param scheduled_time:
    :return:
    """
    start = time.time()

    qotd = None
    try:
        qotd_response = requests.get("http://quotes.rest/qod.json?category=inspire", timeout=5).json()
        logging.log(logging.SEVERITY_DEBUG, "QOTD-QUOTE", qotd_response['contents']['quotes'][0]['quote'])
        logging.log(logging.SEVERITY_DEBUG, "QOTD-AUTHOR", qotd_response['contents']['quotes'][0]['author'])
        qotd_response['contents']['quotes'][0]['quote'] = unicodedata.normalize('NFKD',
                                                                                qotd_response['contents']['quotes'][0][
                                                                                    'quote']).encode('ascii', 'ignore')
        qotd_response['contents']['quotes'][0]['author'] = unicodedata.normalize('NFKD',
                                                                                 qotd_response['contents']['quotes'][0][
                                                                                     'author']).encode('ascii',
                                                                                                       'ignore')
        logging.log(logging.SEVERITY_DEBUG, "QOTD-NORMALIZE", "After processing . . .")
        logging.log(logging.SEVERITY_DEBUG, "QOTD-QUOTE", qotd_response['contents']['quotes'][0]['quote'])
        logging.log(logging.SEVERITY_DEBUG, "QOTD-AUTHOR", qotd_response['contents']['quotes'][0]['author'])
        # quote = ''.join([i if ord(i) < 128 else ' ' for i in qotd_response['contents']['quotes'][0]['quote']])
        # author = ''.join([i if ord(i) < 128 else ' ' for i in qotd_response['contents']['quotes'][0]['author']])
        qotd = (qotd_response['contents']['quotes'][0]['quote'],
                qotd_response['contents']['quotes'][0]['author'])
    except Exception:
        logging.log(logging.SEVERITY_DEBUG, "QOTD-ERROR", traceback.format_exc())

    mongo_conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
    mongo_db = mongo_conn[conf.MONGO_DB_ALT_JINIE]
    mongo_collection_users = mongo_db[conf.MONGO_COLL_USERS]

    # debug = True
    # if debug:
    #     results = mongo_collection_users.find(
    #         {"alt_id": 1802800, "allowed_services.enabled": True, "created_on": {"$exists": True}},
    #         {"alt_id": 1, "allowed_services": 1})
    # else:
    time_zone = str(get_localzone())
    results = mongo_collection_users.find(
        {"allowed_services.enabled": True, "timezone": time_zone, "created_on": {"$exists": True}},
        {"alt_id": 1, "allowed_services": 1, "timezone": 1})

    activemq_conn = stomp.Connection(conf.ACTIVEMQ_CONNECTION)
    activemq_conn.start()
    activemq_conn.connect()
    count = 0

    for result in results:
        count = count + 1
        message = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_REQUEST)
        message['message_type'] = alt_jinie_topics.SCHEDULING
        message['allowed_services'] = result['allowed_services']
        message['destination_alt_user_id'] = result['alt_id']
        message['scheduled_time'] = scheduled_time
        message['user_information'] = result
        message['qotd'] = qotd

        headers = {
            'persistent': 'true'
        }

        activemq_conn.send(destination=conf.ACTIVE_MQ_USER_SCHEDULING_QUEUE,
                           body=optimizations.json_dumps_minify(message),
                           headers=headers)

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "BUILD-USER-Q",
                "Successfully enqueued " + str(count) + " users in " + str(end - start) + " seconds.")


if __name__ == "__main__":
    logging.log(logging.SEVERITY_DEBUG, "BUILD-USER-Q", "Starting")
    SCHEDULE_ON = None
    if len(sys.argv) >= 2:
        try:
            SCHEDULE_ON = datetime.strptime(sys.argv[1], "%d-%m-%YT%H:%M:%S")
            logging.log(logging.SEVERITY_DEBUG, "BUILD-USER-Q",
                        "Successfully parsed scheduled date / time: " + str(SCHEDULE_ON))
        except Exception:
            logging.log(logging.SEVERITY_CRITICAL, "USER-Q-WORKER", traceback.format_exc(),
                        {'schedule_on': SCHEDULE_ON})
            SCHEDULE_ON = None

    if SCHEDULE_ON is None:
        MOTD_DEADLINE = datetime.strptime('08:00:00', '%H:%M:%S').time()
        SCHEDULE_ON = datetime.combine(datetime.today().date(), MOTD_DEADLINE)

    logging.log(logging.SEVERITY_DEBUG, "BUILD-USER-Q", "Enqueuing users for scheduling notifications",
                {'schedule_on': SCHEDULE_ON})

    enqueue_user_from_mongo_to_motd_q(config, SCHEDULE_ON)
