"""Run Process User Queue"""
import json
import time
import traceback
from datetime import datetime

from common import alt_jinie_topics, activemq_helpers, logging
from simplex_communications import config
from reminders import qilo_reminders
from onboarding import organization_onboarding
from simplex_communications.event_producers import scheduler


def proc_user_q_worker():
    """
    Proc User Queue Worker
    :return:
    """

    def work(headers, message, message_id, subscription, activemq_handle):
        """
        Work
        :param headers: Header
        :param message: Message
        :param message_id: Messageid
        :param subscription: Subscription
        :param activemq_handle: Activemq Handle
        :return:
        """

        variable_stack = dict()
        variable_stack['headers'] = headers
        variable_stack['message_id'] = message_id
        variable_stack['subscription'] = subscription
        variable_stack['message'] = message

        logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "Picked up user's request",
                    variable_stack)
        start = time.time()

        try:
            message_dict = json.loads(message)
            if message_dict['message_type'] == alt_jinie_topics.SCHEDULING:
                logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "SCHEDULING Message Type")
                alt_id = message_dict['destination_alt_user_id']
                allowed_services = message_dict['allowed_services']
                user_information = message_dict.pop("user_information", {})
                scheduled_on = datetime.strptime(message_dict['scheduled_time'], "%Y-%m-%d %H:%M:%S")
                additional_info = {'qotd': message_dict.pop("qotd", "None")}
                logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "Before Scheduling response")
                scheduler.schedule(alt_id, user_information, allowed_services, config, False, True, activemq_handle,
                                   scheduled_on, additional_info)
                logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "Before Scheduling response")
            elif message_dict['message_type'] == alt_jinie_topics.QILO_MNGR_REMINDER_PROC:
                logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "SCHEDULING Message Type QILO_MNGR_REMINDER_PROC")
                qilo_reminders.deque_qilo_reminder(message_dict)
            elif message_dict['message_type'] == alt_jinie_topics.ORGANIZATION_ONBOARDING:
                logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "SCHEDULING Message Type ORGANIZATION_ONBOARDING")
                organization_onboarding.onboard_sync_user(message_dict)
            else:
                logging.log(logging.SEVERITY_ERROR, "USER-Q-WORKER", "Unknown Message Type")
        except Exception:
            logging.log(logging.SEVERITY_CRITICAL, "USER-Q-WORKER", traceback.format_exc(),
                        variable_stack)

        end = time.time()
        logging.log(logging.SEVERITY_DEBUG, "USER-Q-WORKER", "Took: " + str(end - start) + " seconds")

    return work


def main():
    """
    Main Function
    :return:
    """
    name = "User Scheduling Queue Consumer"
    activemq_helpers.run_worker(name, proc_user_q_worker(), config,
                                config.ACTIVE_MQ_USER_SCHEDULING_QUEUE)


if __name__ == "__main__":
    main()
