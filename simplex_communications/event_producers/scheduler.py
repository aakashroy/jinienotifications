"""This module schedules messages"""
import time
import traceback
from datetime import datetime, timedelta

import croniter as croniter

from common import logging,o365_helpers
from common.plugin_helpers import PluginClass
from simplex_communications.event_producers import create_plugin_notfns, create_calendar_notfns, create_motd
from tzlocal import get_localzone


def cron_checker(cron_string, time_zone=None):
    """Check CRON"""
    if cron_string is None:
        return None,
    if not cron_string.strip():
        return 0
    cron_string = cron_string.strip()
    if time_zone is None:
        now = datetime.now()
    else:
        now = o365_helpers.convert_datetime_timezone(datetime.utcnow(), "UTC", time_zone)
    dow = now.weekday() + 1
    date_range = []
    if cron_string[-1] != "*":
        try:
            dow = int(cron_string[-1])
        except:
            pass
    try:
        day_setting = cron_string.split(" ")[2]
        if "-" in day_setting or "," in day_setting:
            split_on_comma = day_setting.split(",")
            for comma_splitted_part in split_on_comma:
                split_on_dash = comma_splitted_part.split("-")
                date_range.extend(range
                                  (min(int(split_on_dash[0]), int(split_on_dash[1])),
                                   max(int(split_on_dash[0]), int(split_on_dash[1])) + 1))
    except:
        pass

    fn_response = None
    try:
        cron = croniter.croniter(cron_string, now)
        next_date = cron.get_next(datetime)
        logging.log(logging.SEVERITY_DEBUG, "CRON-NEXT-SCHEDULE",
                    "Next scheduling date is: " + str(next_date))
        # print next_date
        # The second check is to enforce "and" relationship with weekday
        if next_date.date() == now.date() \
                and (now.weekday() + 1 == dow) \
                and (not date_range or now.day in date_range):
            diff = next_date - now
            fn_response = max(diff.total_seconds(), 0)
        else:
            fn_response = -1
    except:
        pass

    return fn_response


def schedule(alt_id, db_user_data, allowed_services, config, onboarding, is_existing_user, activemq_handle=None,
             scheduled_on=None, additional_info=None):
    """
    Schedule Message
    :param alt_id: Altid
    :param db_user_data: User Data
    :param allowed_services: Allowed Services
    :param config: Configuration
    :param onboarding: Onboarding
    :param is_existing_user: Is User already Existing
    :param activemq_handle: Activemq Handle
    :param scheduled_on: Scheduled on
    :param additional_info: Additional information for e.g. Quote of the day, if available
    :return:
    """
    start = time.time()
    user_data = None
    events = None
    if scheduled_on is None:
        scheduled_on = datetime.now()

    variable_stack = dict()
    variable_stack['alt_id'] = alt_id
    db_user_data.pop("allowed_services", "")
    variable_stack['db_user_data'] = db_user_data
    variable_stack['allowed_services'] = allowed_services
    variable_stack['scheduled_on'] = scheduled_on
    user_timezone = db_user_data.get("timezone", str(get_localzone()))
    if user_timezone == '':
        user_timezone = str(get_localzone())
    variable_stack['timezone'] = user_timezone

    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER", "Started Scheduling",
                variable_stack)
    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER", "UserData Scheduling",
                db_user_data)

    try:
        if 'motd' in allowed_services \
                and allowed_services['motd'].get("enabled", False) is True:
            cron = allowed_services['motd'].get("schedule", "").strip()
            if onboarding:
                next_run = 0
            else:
                next_run = cron_checker(cron, user_timezone)
            if next_run is not None:
                if next_run >= 0:
                    next_run = next_run * 1000  # Convert to ms
                    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-MOTD",
                                "Before MOTD")
                    qotd = additional_info.get("qotd", None) if additional_info is not None else None
                    user_data, events = create_motd.create_motd_and_push(alt_id, activemq_conn=activemq_handle,
                                                                         scheduled_delay=next_run,
                                                                         onboarding=onboarding,
                                                                         qotd=qotd)
                    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-MOTD",
                                "After MOTD")
                else:
                    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-MOTD",
                                "Not the correct time for motd to run")
            else:
                logging.log(logging.SEVERITY_ERROR
                            , "SCHEDULER-MOTD",
                            "Looks like schedule has been set incorrectly for this user!")
        else:
            logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-MOTD",
                        "MOTD not enabled")
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "SCHEDULER-MOTD",
                    traceback.format_exc(), variable_stack)

    try:
        if 'event_notifications' in allowed_services \
                and allowed_services['event_notifications'].get("enabled", False) is True:

            # This means that either a new user is onboarding
            # OR an existing user is being scheduled
            if (not is_existing_user and onboarding) or (is_existing_user and not onboarding):
                logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-EVENTS",
                            "Before Event Notifications")
                user_data, events = create_calendar_notfns.create_calendar_notfns_and_push(alt_id,
                                                                                           activemq_conn=activemq_handle,
                                                                                           user_data=user_data,
                                                                                           events=events)
                logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-EVENTS",
                            "After Event Notifications")
            else:
                logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-EVENTS",
                            "Not the correct time for event_notifications to run", {"onboarding": onboarding,
                                                                                    "is_existing_user": is_existing_user})
        else:
            logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-EVENTS",
                        "Event Notifications not allowed")
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "SCHEDULER-EVENTS",
                    traceback.format_exc(), variable_stack)

    plugin_obj = PluginClass()
    plugins = plugin_obj.get_plugins(config.PLUGIN_DIR, config.PLUGIN_PKG_NAME,
                                     config.PLUGIN_RUN_METHOD_NAME,
                                     config.PLUGIN_SVC_NAME_METHOD_NAME)
    for (run_fn, svc_name_fn) in plugins:
        service_name = svc_name_fn()
        if service_name in allowed_services \
                and allowed_services[service_name].get("enabled", False) is True:
            cron = allowed_services[service_name].get("schedule", "").strip()
            next_run = cron_checker(cron)
            if next_run is not None:
                if next_run >= 0:
                    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                                "Correct time for " + service_name + " to run")
                    next_run = next_run * 1000  # Convert to ms
                    try:
                        logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                                    "Before Plugin Notfn")
                        message = create_plugin_notfns.get_plugin_notfn_message(alt_id, run_fn, user_data,
                                                                                scheduled_on)
                        if message:
                            create_plugin_notfns.send(alt_id, message, next_run, activemq_handle)
                            logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                                        "After Plugin Notfn", {"message": message,
                                                               "schedule": str(datetime.now() + timedelta(
                                                                   milliseconds=next_run))})
                        else:
                            logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                                        "After Plugin Notfn")
                    except:
                        logging.log(logging.SEVERITY_CRITICAL, "SCHEDULER-PLUGINS",
                                    traceback.format_exc(), variable_stack)
                else:
                    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                                "Not the correct time for " + service_name + " to run")
            else:
                logging.log(logging.SEVERITY_ERROR
                            , "SCHEDULER-PLUGINS",
                            "Looks like schedule has been set incorrectly for this user!")
        else:
            logging.log(logging.SEVERITY_DEBUG, "SCHEDULER-PLUGINS",
                        service_name + " not in allowed_services OR disabled.")

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "SCHEDULER", "Took: " + str(end - start) + " seconds")

if __name__ == "__main__":
    cron_checker("0 8 * * *", "Asia/Kolkata")