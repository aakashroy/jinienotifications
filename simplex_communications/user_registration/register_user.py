# -*- coding: utf-8 -*-
"""Register new user"""
import copy
import time
import traceback
from datetime import datetime

import pymongo
import requests

from common import mongo_helpers, logging
from common.o365_helpers import register_email_created_webhook
from simplex_communications import config
from simplex_communications.event_producers import scheduler


def onboarding_request_handler(alt_id, emp_id, allowed_services, onboarding_profile, tokens, activemq_handle):
    """
    This function is to handle onboarding requests
    :param alt_id: Altid
    :param emp_id: Employee id
    :param allowed_services: Allowed Services
    :param onboarding_profile:
    :param tokens: Token
    :param activemq_handle: Activemq Handle
    :return:
    """
    mongo_success = False

    variable_stack = dict()
    variable_stack['alt_id'] = alt_id
    variable_stack['emp_id'] = emp_id
    variable_stack['allowed_services'] = allowed_services
    variable_stack['onboarding_profile'] = onboarding_profile
    variable_stack['tokens'] = tokens

    logging.log(logging.SEVERITY_DEBUG, "ONBOARDING-REQUEST-HANDLER", "Processing user data to be uploaded to MongoDB")
    start = time.time()

    user_details = mongo_helpers.get_user_details_by_alt_id(alt_id, config)
    variable_stack.update(user_details)
    if user_details is not None:
        is_existing_user = False
        if 'created_on' in user_details:
            # More than 36 hours elapsed?
            # if (datetime.now() - user_details['updated_on']).total_seconds() <= 129600:
            is_existing_user = True

        variable_stack['send_calendar'] = is_existing_user

        logging.log(logging.SEVERITY_DEBUG, "ONBOARDING-REQUEST-HANDLER",
                    "User fetched from MongoDB", variable_stack)

        user_information = dict()

        try:
            user_information['alt_id'] = alt_id
            user_information['emp_id'] = emp_id
            user_information['token_type'] = tokens['token_type']
            user_information['scopes'] = tokens['scope'].strip()
            user_information['access_token'] = tokens['access_token']
            user_information['refresh_token'] = tokens['refresh_token']
            user_information['id_token'] = tokens['id_token']
            user_information['expires_in'] = tokens['expires_in']
            user_information['ext_expires_in'] = tokens['ext_expires_in']

            user_information['ms_id'] = onboarding_profile['id']

            phones = []
            if onboarding_profile.get("mobilePhone") is not None:
                phones.extend(onboarding_profile["mobilePhone"])
            if onboarding_profile.get("businessPhones") is not None:
                phones.extend(onboarding_profile["businessPhones"])
            if phones:
                user_information["phones"] = phones

            user_information['full_name'] = onboarding_profile['displayName']

            # if onboarding_profile.get("givenName") is not None and onboarding_profile.get('givenName'):
            #     user_information['first_name'] = onboarding_profile['givenName']

            # if onboarding_profile.get("surname") is not None and onboarding_profile.get('surname'):
            #     user_information['last_name'] = onboarding_profile['surname']

            if onboarding_profile.get("jobTitle") is not None and onboarding_profile.get('jobTitle'):
                user_information['job_title'] = onboarding_profile['jobTitle']

            if user_details["email"] == onboarding_profile['mail']:
                user_information['email'] = onboarding_profile['mail']
            else:
                user_information['email'] = onboarding_profile['userPrincipalName']

            user_information['mail_ms'] = onboarding_profile['mail']
            user_information['user_principal_name'] = onboarding_profile['userPrincipalName']

            if onboarding_profile.get("officeLocation") is not None and onboarding_profile.get("officeLocation"):
                user_information['office_location'] = onboarding_profile['officeLocation']

            if onboarding_profile.get("preferredLanguage") is not None and onboarding_profile.get("preferredLanguage"):
                user_information['preferred_language'] = onboarding_profile['preferredLanguage']

            user_information['created_on'] = datetime.now()
            user_information['updated_on'] = user_information['created_on']

            # Not needed since this will already happen when creating MOTD
            # tmp_usr_info = copy.deepcopy(user_details)
            # tmp_usr_info.update(user_information)
            # response = register_email_created_webhook(tmp_usr_info, config)
            # if response and "id" in response:
            #     user_information["o365_email_created_subscription_id"] = response["id"]
            #     user_information["o365_email_created_subscription_creator_id"] = response["creatorId"]

            variable_stack.update(user_information)

            mongo_conn = pymongo.MongoClient(config.MONGO_CONNECTION)
            mongo_db = mongo_conn[config.MONGO_DB_ALT_JINIE]
            mongo_collection_users = mongo_db[config.MONGO_COLL_USERS]

            select_clause = {"alt_id": alt_id}
            update_clause = {"$set": user_information}
            mongo_collection_users.update_one(select_clause, update_clause, upsert=True)
            mongo_success = True
        except Exception:
            logging.log(logging.SEVERITY_CRITICAL, "ONBOARDING-REQUEST-HANDLER-MONGO-INSERT",
                        traceback.format_exc(), variable_stack)

        if mongo_success:
            logging.log(logging.SEVERITY_DEBUG, "ONBOARDING-REQUEST-HANDLER", "Before Scheduling response")
            scheduler.schedule(alt_id, user_information, allowed_services, config, True, is_existing_user,
                               activemq_handle)
            logging.log(logging.SEVERITY_DEBUG, "ONBOARDING-REQUEST-HANDLER", "After Scheduling response")
        else:
            logging.log(logging.SEVERITY_ERROR, "ONBOARDING-REQUEST-HANDLER", "Not scheduling response")

    else:
        logging.log(logging.SEVERITY_ERROR, "ONBOARDING-REQUEST-HANDLER", "User not found in MongoDB")

    end = time.time()

    logging.log(logging.SEVERITY_DEBUG, "ONBOARDING-REQUEST-HANDLER",
                "Took: " + str(end - start) + " seconds")


if __name__ == "__main__":
    qotd_response = requests.get("http://quotes.rest/qod.json?category=inspire", timeout=5).json()
    logging.log(logging.SEVERITY_DEBUG, "QOTD-QUOTE", qotd_response['contents']['quotes'][0]['quote'])
    logging.log(logging.SEVERITY_DEBUG, "QOTD-AUTHOR", qotd_response['contents']['quotes'][0]['author'])
    # quote = ''.join([i if ord(i) < 128 else ' ' for i in qotd_response['contents']['quotes'][0]['quote']])
    # author = ''.join([i if ord(i) < 128 else ' ' for i in qotd_response['contents']['quotes'][0]['author']])
    qotd = (qotd_response['contents']['quotes'][0]['quote'],
            qotd_response['contents']['quotes'][0]['author'])
    USER_DATA = mongo_helpers.get_user_details_by_alt_id(2205395, config)
    scheduler.schedule(2205395, USER_DATA, USER_DATA['allowed_services'], config, True, True,
                       additional_info={"qotd": qotd})
