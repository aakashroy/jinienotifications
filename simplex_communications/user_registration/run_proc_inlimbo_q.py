"""Process Inlimbo Queue"""
import json
import time
import traceback

from common import alt_jinie_topics, activemq_helpers, logging
from simplex_communications import config
from simplex_communications.user_registration import register_user


def proc_inlimbo_q_worker():
    """
    Proc inlimbo Queue Worker
    :return:
    """
    def work(headers, message, message_id, subscription, activemq_handle):
        """
        Work
        :param headers: Headers
        :param message: Message
        :param message_id: Message id
        :param subscription: Subscription
        :param activemq_handle: Activemq Handle
        :return:
        """
        variable_stack = dict()
        variable_stack['headers'] = headers
        variable_stack['message_id'] = message_id
        variable_stack['subscription'] = subscription
        variable_stack['message'] = message
        logging.log(logging.SEVERITY_DEBUG, "IN-LIMBO-Q-WORKER", "Picked up user's request",
                    variable_stack)
        start = time.time()
        try:
            message_dict = json.loads(message)
            if message_dict['message_type'] == alt_jinie_topics.ONBOARDING:
                logging.log(logging.SEVERITY_DEBUG, "IN-LIMBO-Q-WORKER", "ONBOARDING Message Type")
                alt_id = message_dict['destination_alt_user_id']
                emp_id = message_dict['emp_id']
                allowed_services = message_dict['allowed_services']
                register_user.onboarding_request_handler(alt_id, emp_id, allowed_services, message_dict['me'],
                                                         message_dict['tokens'], activemq_handle)
            else:
                logging.log(logging.SEVERITY_ERROR, "IN-LIMBO-Q-WORKER", "Unknown Message Type")
        except:
            logging.log(logging.SEVERITY_CRITICAL, "IN-LIMBO-Q-WORKER", traceback.format_exc(),
                        variable_stack)

        end = time.time()
        logging.log(logging.SEVERITY_DEBUG, "IN-LIMBO-Q-WORKER", "Took: " + str(end - start) + " seconds")

    return work


def main():
    """
    Main function
    :return:
    """
    name = "User Inlimbo Queue Consumer"
    activemq_helpers.run_worker(name, proc_inlimbo_q_worker(), config,
                                config.ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE)


if __name__ == "__main__":
    main()
