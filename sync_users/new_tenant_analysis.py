import json

from plugins.common.sql_helpers import connect_to_sql_server

QUERY = "select hre.OfficialEmailAddress, su.FirstName, su.MiddleName, su.LastName, \
        hre.UserId, hre.EmployeeID \
        from dbo.HrEmployee as hre \
        inner join dbo.SysUser as su on su.UserID=hre.UserID \
        where hre.OrganizationId=%s and hre.EmploymentStatusID in %s \
        and hre.OfficialEmailAddress is not NULL"


def get_outlier_line(user, reason):
    return [
        user[4],
        user[5],
        user[0],
        reason
    ]


def analyze_new_tenant(org_id, emp_status_ids):
    outliers = list()
    sql_con = connect_to_sql_server()
    sql_con.execute(QUERY, (org_id, emp_status_ids))
    users_to_analyze = sql_con.fetchall()

    allowed_domains = dict()
    empty_emails = []
    empty_alt_id = 0
    empty_emp_id = 0
    empty_first_names = 0

    for user in users_to_analyze:
        email = user[0]
        email = email.strip() if email else ""
        if email:
            domain = email.split("@")
            cnt = allowed_domains.get(domain[-1], 0)
            cnt = cnt + 1
            allowed_domains[domain[-1]] = cnt
        else:
            empty_emails.append((user[1], user[2], user[3]))
            outliers.append(get_outlier_line(user, "Empty Email Address"))
        if not user[1]:
            empty_first_names = empty_first_names + 1
            outliers.append(get_outlier_line(user, "Empty First Name"))
        if user[4] <= 0:
            empty_alt_id = empty_alt_id + 1
            outliers.append(get_outlier_line(user, "Invalid User Id"))
        if user[5] <= 0:
            empty_emp_id = empty_emp_id + 1
            outliers.append(get_outlier_line(user, "Invalid Employee Id"))

    allowed_domains_sorted = sorted(allowed_domains.items(), key=lambda x: x[1])

    for i in range(1, len(allowed_domains_sorted)):
        outliers.append(get_outlier_line(user, "Check Email for Spelling / Domain Errors"))

    print "Allowed Domains: "
    print json.dumps(allowed_domains, indent=4)
    print "Empty Emails: "
    print len(empty_emails)
    print "Empty Email Names: "
    print json.dumps(empty_emails, indent=4)
    print "Empty First Names: "
    print empty_first_names
    print "Empty Alt IDs: "
    print empty_alt_id
    print "Empty Employee IDs: "
    print empty_emp_id

    # fields = ['Name', 'Branch', 'Year', 'CGPA']
    # with open("outliers-tenant-analysis-" + str(org_id) + ".csv", 'w') as csvfile:
    #     csvwriter = csv.writer(csvfile)

if __name__ == "__main__":
    analyze_new_tenant([9], [181, 5845])
