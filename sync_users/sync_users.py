"""This module syncs users from Talentpact ATS DB to MongoDB"""
import socket
import traceback
from datetime import datetime

from pspython import email_functions

from common.mongo_helpers import fetch_all_user_alt_ids, insert_new_user, delete_user
from plugins.common.sql_helpers import connect_to_sql_server
from simplex_communications import config

# (210,214)
QUERY = "select hre.EmployeeID, hre.OfficialEmailAddress, hre.UserId, su.FirstName, su.MiddleName, su.LastName, hre.OrganizationId \
        from dbo.HrEmployee as hre \
        inner join dbo.SysUser as su on su.UserID=hre.UserID \
        where hre.OrganizationId=%s and hre.EmploymentStatusID in %s \
        and hre.OfficialEmailAddress is not NULL"


def local_logger(old_log_string, new_log_string):
    """

    A local logger for collating logs and sending as email.

    :param old_log_string: The log string already saved so far
    :param new_log_string: The new string to append
    :return: Updated log string
    """
    log_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print log_time + " | " + new_log_string
    return old_log_string + u"<br/>" + log_time + u" | " + new_log_string


def sync(orgIds, emp_status_ids, config):
    """

    :param orgIds: Organization Ids to sync
    :param config: Configuration
    :return:
    """

    email_log = ""
    email_log = local_logger(email_log, "Starting Sync with Organization IDs: " + str(orgIds))
    for orgId in orgIds:
        email_log = local_logger(email_log, "Now syncing Organization ID: " + str(orgId))
        try:
            sql_con = connect_to_sql_server()
            email_log = local_logger(email_log, "Connected to TalentPact DB")
            sql_con.execute(QUERY, (orgId, emp_status_ids))
            email_log = local_logger(email_log, "Query to fetch users executed")
            users_to_sync = sql_con.fetchall()

            email_log = local_logger(email_log, str(len(users_to_sync)) + " users fetched from TalentPact DB")

            users_in_sql = set([x[2] for x in users_to_sync])

            users_in_sql_lookup_dict = {z: (x, y, z, f, m, l, n) for (x, y, z, f, m, l, n) in users_to_sync}

            users_in_mongo_cursor = fetch_all_user_alt_ids(config, orgId)
            users_in_mongo = set()
            for mongo_user in users_in_mongo_cursor:
                users_in_mongo.add(mongo_user['alt_id'])

            email_log = local_logger(email_log, str(len(users_in_mongo)) + " users fetched from Mongo DB")

            users_to_add = users_in_sql.difference(users_in_mongo)
            email_log = local_logger(email_log, str(len(users_to_add)) + " users to add")
            users_to_del = users_in_mongo.difference(users_in_sql)
            email_log = local_logger(email_log, str(len(users_to_del)) + " users to remove")

            email_log = local_logger(email_log, "Deleting users . . .")

            deleted_user_success_count = 0
            deleted_user_error_count = 0
            for user_id in users_to_del:
                print "Deleting: " + str(user_id)
                if delete_user(config, user_id):
                    deleted_user_success_count = deleted_user_success_count + 1
                else:
                    deleted_user_error_count = deleted_user_error_count + 1

            email_log = local_logger(email_log, "Adding users . . .")

            inserted_user_success_count = 0
            inserted_user_error_count = 0
            for user_id in users_to_add:
                email = users_in_sql_lookup_dict[user_id][1]
                email = email.strip() if email else ""
                if email:
                    print "Email of User: " + str(email)
                    print "Inserting: " + str(user_id)
                    if insert_new_user(config,
                                       users_in_sql_lookup_dict[user_id][6],
                                       users_in_sql_lookup_dict[user_id][2],
                                       users_in_sql_lookup_dict[user_id][0],
                                       users_in_sql_lookup_dict[user_id][1],
                                       users_in_sql_lookup_dict[user_id][3],
                                       users_in_sql_lookup_dict[user_id][4],
                                       users_in_sql_lookup_dict[user_id][5]
                                       ):
                        inserted_user_success_count = inserted_user_success_count + 1
                    else:
                        inserted_user_error_count = inserted_user_error_count + 1
                else:
                    print "Not Inserting | Email of User: " + str(email)

            email_log = local_logger(email_log, str(inserted_user_success_count) + " users added")
            email_log = local_logger(email_log, str(inserted_user_error_count) + " errors while adding users")
            email_log = local_logger(email_log, str(deleted_user_success_count) + " users removed")
            email_log = local_logger(email_log, str(deleted_user_error_count) + " errors while removing users")

        except Exception:
            email_log = local_logger(email_log, traceback.format_exc())

    print email_log
    print email_functions.send_mail("altml@alt.peoplestrong.com", "Alt Jinie Notification System", [{
        'recipient_email': 'aakash.roy@peoplestrong.com',
        'subject': 'Activity Log | Jinie Notifications User Sync | ' + socket.gethostname(),
        'body': 'Hi, Activity log is below: <hr/>' + email_log + "<hr/><br/>Best,<br/>Jinie | I'm the nu cool!"
    }])


if __name__ == "__main__":
    #sync([19], [210, 214], config)
    sync([9], [181, 5845], config)
